window.onkeydown = function(e){
	switch(e.keyCode){
		case 13:
			if(Dialog.getAttribute("data-type") == "RS"){
				gn("RS_OKBtn").onclick();
			}
			break;
		case 27:
			if(Dialog.getAttribute("data-type") == "RS"){
				dialog();
			}
			break;
		case 112: // F1 패스			즉발
		case 113: // F2 점프			택1
		case 114: // F3 뒤로			택1
		case 115: // F4 한번더		택1
			if(isGaming){
				toggleItem(e.keyCode-112);
				e.preventDefault();
				return;
			}
			break;
		default:
	}
}
window.onkeyup = function(e){
	if(MyTurnDiv.style.display == ""){
		var cv = ChatInput.value.length;
		var sw = window.innerWidth*0.5-10;
		MyTurnText.value = ChatInput.value;
		var cf = 36;
		var ck;
		do{
			ck = 0.5*cf*cv;
			cf--;
			if(cf == 12) break;
		}while(ck > sw);
		MyTurnText.style.fontSize = cf+"px";
	}
}