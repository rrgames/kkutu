class Conquest
	attr_accessor :score
	attr_accessor :rating
	attr_accessor :ping
	attr_accessor :normalPlay
	attr_accessor :normalWin
	attr_accessor :rankedPlay
	attr_accessor :rankedWin
	attr_accessor :typeTime
	attr_accessor :typeScore
	attr_accessor :description
	
	# 전적 정보는 배열이다.
	
	def initialize()
	end
	def toObject()
		{
			:score => @score,
			:rating => @rating,
			:ping => @ping,
			:normalPlay => @normalPlay,
			:normalWin => @normalWin,
			:rankedPlay => @rankedPlay,
			:rankedWin => @rankedWin,
			:typeTime => @typeTime,
			:typeScore => @typeScore,
			:description => @description
		}
	end

end