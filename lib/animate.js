var ani = {
	CON: {
		ITEM_LIST: ["패스", "끝말잇기", "찬스"," 점프", "뒤로", "한번더"]
	}
}

function aWrong(arg, repeat){
	displaySubject(arg, (repeat%2)?"normal":"aWrong");
	if(repeat > 0){
		ani.wt = setTimeout(aWrong, 300, arg, repeat-1);
	}else{
		displaySubject(ani.wq);
	}
}
function aAccept(arg){
	var tl;
	var cw;
	var b2 = (arg.charCodeAt()>255)?2:1;
	var tv = getTimeLevel(g.mtt);
	var tq = 80-tv*5;
	if(arg){
		tl = arg.length;
		var PS = window.innerWidth*0.5-27;
		cw = Math.max(8, 24-(tl*160)/PS);
		GameSubjectPanel.innerHTML = "　";
		ani.wc = new Array();
		var ch = Math.round((82-cw*1.5)*0.5);
		var WC = (window.innerWidth*0.5-tl*cw*b2)*0.5-5;
		var soundV = (tl>=10)?"WW":"W";
		
		var mq = (my.room.config.rule == "KT")?3:1;
		if(RULE_TYPE[my.room.config.rule] != "eng"){
			WC += 10;
		}
		var ts = "W";
		if(my.room.config.rule == "KK"){
			// 1200 1095 990 885 780 675 570 465 360 255 150
			tq = 400-35*tv;
			soundV = "WK"+tv;
			playSound(soundV, ts);
		}

		for(var i=0; i<tl; i++){
			var D = document.createElement("label");
			D.className = "AChar";
			D.style.top = ch+"px";
			D.style.fontSize = "0px";
			D.innerHTML = arg.charAt(i);
			GameSubjectPanel.appendChild(D);
			var no = {
				id: i,
				left: WC+i*cw*b2,
				size: cw*3,
				goalSize: cw*1.5,
				alpha: 1,
				obj: D
			}
			D.style.left = no.left+"px";
			ani.wc.push(no);
			if(my.room.config.rule != "KK"){
				setTimeout(playSound, i*tq, soundV, ts);
			}
			setTimeout(_aAccept, i*tq - 10, no);
			setTimeout(aClean, Math.round(1440+i*tq*0.625-tv*103), no, tl-i > mq, (mq==3)?3:2);
		}
	}else{
		tl = 0;
	}
	setTimeout(requestNextTurn, Math.round(1500+tl*tq*0.625-tv*103));
}
function _aAccept(o){
	if(o.size < o.goalSize+2){
		o.size = o.goalSizegoalSize;
	}else{
		o.size += (o.goalSize-o.size)*0.3;
		ani.at = setTimeout(_aAccept, 30, o);
	}
	o.obj.style.fontSize = Math.round(o.size)+"px";
}
function aClean(o, gone, lv){
	var og = gone?0:(window.innerWidth*0.25-29+16*(o.id-ani.wc.length+lv));
	if(o.left < 10){
		o.obj.style.display = "none";
	}else{
		o.left += (og-o.left)*0.5;
		ani.act = setTimeout(aClean, 30, o, gone, lv);
	}
	o.obj.style.left = Math.round(o.left)+"px";
}
function aAppear(div){
	ani.aa = 0;
	div.style.opacity = ani.aa;
	ani.at = setTimeout(_aAppear, 30, div);
}
function _aAppear(div){
	ani.aa += 0.1;
	div.style.opacity = ani.aa;
	if(ani.aa >= 1){
		ani.aa = 1;
	}else{
		ani.at = setTimeout(_aAppear, 30, div);
	}
}
function aScore(arg){
	arg.alpha -= 0.012;
	if(arg.font > 20){
		arg.font += arg.dF;
		arg.dF -= 0.4;
	}else{
		arg.font = 20;
	}
	arg.top -= (1-arg.alpha)*4;
	if(arg.alpha > 0){
		ani.st = setTimeout(aScore, 30, arg);
		arg.validateDiv();
	}else{
		arg.div.parentElement.removeChild(arg.div);
	}
}
function aParticle(particle){
	var dX = particle.gX - particle.x;
	var dY = particle.gY - particle.y;
	if(Math.abs(dX) + Math.abs(dY) < 2){
		particle.x = particle.gX;
		particle.y = particle.gY;
		particle.div.parentElement.removeChild(particle.div);
	}else{
		particle.x += dX*0.2*particle.dmX;
		particle.y += dY*0.2*particle.dmY;
		particle.dmX += 0.1;
		particle.dmY += 0.1;
		particle.alpha -= 0.03;
		setTimeout(aParticle, 30, particle);
	}
	particle.validateDiv();
}
function aStopBGM(){
	if(SC.bgm.volume > 0){
		SC.bgm.volume = Math.max(0, SC.bgm.volume-0.03);
		ani.sb = setTimeout(aStopBGM, 30);
	}else{
		SC.bgm.src = "";
	}
}
function aEarthquake(level){
	ani.eql = level;
	ani.eqx = 30;
	_aEarthquake(level);
}
function _aEarthquake(){
	ani.eqx += ani.eql*2;
	ani.eql *= -0.9;
	if(Math.abs(ani.eql) < 1){
		ani.eqx = 30;
	}else{
		ani.eqt = setTimeout(_aEarthquake, 80);
	}
	Middle.style.top = Math.round(ani.eqx)+"px";
}
function aResult(){
	ani.rby = -50;
	_aResult();
}
function _aResult(){
	var delta = 50-ani.rby;
	ani.rby += delta*0.3;
	if(delta < 1){
		ani.rby = 50;
		ani.rbt = setTimeout(aGetExp, 1000);
	}else{
		ani.rbt = setTimeout(_aResult, 30);
	}
	GameResultBox.style.top = "calc("+Math.round(ani.rby)+"% - 180px)";
}
function aItemEffect(item){
	clearInterval(ani.aet);
	ani.aey = 280;
	item.style.top = ani.aey+"px";
	_aItemEffect(item);
}
function _aItemEffect(item){
	ani.aey -= 1;
	item.style.top = Math.round(ani.aey)+"px";
	item.style.opacity = (ani.aey-180)*0.01;
	if(ani.aey > 0){
		ani.aet = setTimeout(_aItemEffect, 30, item);
	}else{
		item.parentElement.removeChild(item);
	}
}
function aMagnify(item, ratio){
	ani.mfs = ratio || 120;
	if(160-ani.mfs < 1){
		ani.mfs = 160;
	}else{
		ani.mfs += (160-ani.mfs)*0.3;
		ani.mft = setTimeout(aMagnify, 30, item, ani.mfs);
	}
	item.style.width = Math.round(ani.mfs)+"px";
	item.style.height = Math.round(ani.mfs)+"px";
}
function aDemagnify(item, ratio){
	ani.dmfs = ratio || 160;
	if(ani.dmfs-120 < 1){
		ani.dmfs = 120;
	}else{
		ani.dmfs += (120-ani.dmfs)*0.3;
		ani.dmft = setTimeout(aDemagnify, 30, item, ani.dmfs);
	}
	item.style.width = Math.round(ani.dmfs)+"px";
	item.style.height = Math.round(ani.dmfs)+"px";
}
function aDark(alpha){
	alpha = alpha || 1;
	if(alpha > 0.05){
		alpha *= 0.9;
		Dark.style.display = "";
		ani.dt = setTimeout(aDark, 30, alpha);
	}else{
		Dark.style.display = "none";
		alpha = 0;
	}
	Dark.style.opacity = alpha;
}
function aGetExp(c){
	if(c == undefined) c = 0;
	var delta = ani.geMount-c;
	if(delta > 0.01){
		c += delta*0.2;
		ani.get = setTimeout(aGetExp, 30, c);
	}else{
		c = ani.geMount;
	}
	aDrawBothExpBar(ani.geC+c);
}
function aDrawBothExpBar(value){
	var level = getLevel(value);
	var bef = getGoalScore(level-1);
	var aft = getGoalScore(level);
	var er = (value-bef)/(aft-bef);
	//MyLevel.innerHTML = "<label style='font-size: 12px;'>레벨</label>&nbsp;"+level;
	//MyExp.style.width = Math.round(er*106)+"px";
	GameResultExpText.innerHTML = Math.round(value)+" / "+aft;
	GameResultExpBar.style.width = Math.round(er*390)+"px";
}