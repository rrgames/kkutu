class Client
	attr_accessor :id
	attr_accessor :name
	attr_accessor :socket
	attr_accessor :room
	attr_accessor :game
	attr_accessor :conquest
	attr_accessor :fb
	attr_accessor :hack
	attr_accessor :terminal
	attr_accessor :rival
	# name 속성은 서버 내에서의 닉네임
	# game 속성은 진행하고 있는 게임에 대한 정보들
	# conquest 속성은 사용자의 전반적 게임 정보들
	
	# terminal, rival <String> 속성
	#	terminal에는 rule이 들어감
	#	매칭이 되면 rival에 아이디가 들어감
	#	자신이 HANDSHAKE를 하면 rival 값 가장 앞에 *가 붙음

	# fb <Hash> 속성
	#	:id			페이스북 식별자
	#	:sudo		나는 관리자다
	#	:newbie	신입 회원 여부 (초기 닉네임 설정을 위함)

	# hack <Hash> 속성
	#	:recReq			최근 요청 시각
	#	:blocked			차단 여부
	#	:reqUnblockT	차단이 풀리기 위해 필요한 대기 시간
	#	:level				해킹 레벨
	#	:point				해킹 점수 (일정 수치 이상이 되면 연결 끊음)
	
	def initialize(id)
		@id = id
		@room = "lobby"
		@game = Game.new(self)
		@fb = Hash.new
		@conquest = Conquest.new
		@hack = {
			:recReq => now,
			:blocked => false,
			:reqUnblockT => 0,
			:level => 1,
			:point => 0
		}
	end
	def enter(room)
		@room = room.id
		@game.ready = false
		room.users.push(self)
	end
	def exit(kicked = false)
		if @room == "lobby" then
			return
		end
		myRoom = $rooms[@room]
		publish(@room, {
			:code => Con::EXIT,
			:kicked => kicked,
			:id => @id
		})
		ri = myRoom.users.index { |item| item.id == @id }
		myRoom.users.delete_at(ri)
		ul = myRoom.users.length
		# 방 인원이 0이 되어 방이 폭파되는 경우
		if ul <= 0 then
			myRoom.remove()
			$rooms.delete(@room)
			publish("global", {
				:type => "delete room",
				:code => Con::ROOM_DELETE,
				:roomId => @room
			})
			@room = "lobby"
		else
			@room = "lobby"
			if myRoom.master == @id then
				# 방장이 나갈 경우 방장 인계를 해야됨
				myRoom.master = myRoom.users[0].id
			end
			if @game.playing then
				# 게임 진행 중 나감
				myRoom.game[:conseq].delete_if do |item|
					item == @id
				end
				if myRoom.game[:turn] >= ul then
					myRoom.game[:turn] = 0
				end
				if ul == 1 then
					myRoom.endGame()
				else
					myRoom.startTurn()
				end
			end
			myRoom.publishData(Con::ROOM_UPDATE, @game.playing)
		end
	end
	def block()
		@hack[:blocked] = true
		@hack[:reqUnblockT] = @hack[:level]*10000
		@hack[:level] += 1
		@hack[:point] = 0
		send(@socket, {
			:code => Con::BLOCKED,
			:rest => @hack[:reqUnblockT]
		})
	end
	def flush()
		# 데이터베이스에 정보를 씀
		if @conquest != nil then
			q = "UPDATE `user` SET `score` = #{@conquest.score}, "
			Con::RULE_INDEX.each do |key, value|
				q += "`rating_#{key}` = #{@conquest.rating[value]}, "
				q += "`normalPlay_#{key}` = #{@conquest.normalPlay[value]}, "
				q += "`normalWin_#{key}` = #{@conquest.normalWin[value]}, "
				q += "`rankedPlay_#{key}` = #{@conquest.rankedPlay[value]}, "
				q += "`rankedWin_#{key}` = #{@conquest.rankedWin[value]}, "
				q += "`typeTime_#{key}` = #{@conquest.typeTime[value]}, "
				q += "`typeScore_#{key}` = #{@conquest.typeScore[value]}, "
			end
			q += "`ping` = #{@conquest.ping} WHERE `id` = '#{@fb[:id]}';"
			$dbObj.query(q)
		end
	end
	def fetch(data)
		# 데이터베이스로부터 정보를 받아옴
		# 받아온 정보는 로그아웃할 때 데이터베이스에 올리고 메모리에서 해제
		@fb[:id] = data["id"]
		@fb[:sudo] = Con::SUDO_LIST.index(@fb[:id]) != nil 
		# 색인
		# name			0
		# score			1
		# ping				7
		# description	38
		#					_	KT	ES	KS	KK	JS
		# rating			2		3		4		5		6
		# normalPlay	8		9		10	11	12
		# normalWin	13	14	15	16	17
		# rankedPlay	18	19	20	21	22
		# rankedWin	23	24	25	26	27
		# typeTime		28	29	30	31	32
		# typeScore		33	34	35	36	37
		res = $dbObj.query("SELECT `name`, `score`, `rating_KT`, `rating_ES`, `rating_KS`, `rating_KK`, `rating_JS`, `ping`, `normalPlay_KT`, `normalPlay_ES`, `normalPlay_KS`, `normalPlay_KK`, `normalPlay_JS`, `normalWin_KT`, `normalWin_ES`, `normalWin_KS`, `normalWin_KK`, `normalWin_JS`, `rankedPlay_KT`, `rankedPlay_ES`, `rankedPlay_KS`, `rankedPlay_KK`, `rankedPlay_JS`, `rankedWin_KT`, `rankedWin_ES`, `rankedWin_KS`, `rankedWin_KK`, `rankedWin_JS`, `typeTime_KT`, `typeTime_ES`, `typeTime_KS`, `typeTime_KK`, `typeTime_JS`, `typeScore_KT`, `typeScore_ES`, `typeScore_KS`, `typeScore_KK`, `typeScore_JS`, `description` FROM `user` WHERE `id` = '#{@fb[:id]}';").fetch_row
		if res == nil then
			# 새 유저
			@fb[:newbie] = true
			send(@socket, {
				:code => Con::NEWBIE,
				:def => @name
			})
		else
			$dbObj.query("UPDATE `user` SET `lastLoggedIn` = CURRENT_TIMESTAMP WHERE `id` = '#{@fb[:id]}';")
			@fb[:newbie] = false
			@name = res[0].force_encoding("UTF-8")
			@conquest = Conquest.new
			@conquest.score = res[1].to_f
			@conquest.rating = [res[2].to_i, res[3].to_i, res[4].to_i, res[5].to_i, res[6].to_i]
			@conquest.ping = res[7].to_i
			@conquest.normalPlay = [res[8].to_i, res[9].to_i, res[10].to_i, res[11].to_i, res[12].to_i]
			@conquest.normalWin = [res[13].to_i, res[14].to_i, res[15].to_i, res[16].to_i, res[17].to_i]
			@conquest.rankedPlay = [res[18].to_i, res[19].to_i, res[20].to_i, res[21].to_i, res[22].to_i]
			@conquest.rankedWin = [res[23].to_i, res[24].to_i, res[25].to_i, res[26].to_i, res[27].to_i]
			@conquest.typeTime = [res[28].to_i, res[29].to_i, res[30].to_i, res[31].to_i, res[32].to_i]
			@conquest.typeScore = [res[33].to_i, res[34].to_i, res[35].to_i, res[36].to_i, res[37].to_i]
			@conquest.description = ""
			if res[38] != nil then
				res[38].force_encoding("UTF-8")
			end
		end
	end
	def setNewbie(name)
		# 닉네임 중복 확인
		res = $dbObj.query("SELECT `name` FROM `user` WHERE `name` = '#{name}';").fetch_row
		if res == nil then
			# INSERT한다. 조심!
			$dbObj.query("INSERT INTO `user` (`id`, `name`) VALUES ('#{@fb[:id]}', '#{name}');")
			@fb[:newbie] = false
			fetch({
				"id" => @fb[:id]
			})
			welcome(self)
		else
			# 이미 있는 닉네임
			send(@socket, {
				:code => Con::WARN,
				:text => Msg::NEWBIE_NAME_OCCUPIED
			})
		end
	end
	def toObject(full = true)
		res = {
			:id => @id,
			:name => @name,
			:status => status,
			:game => @game.toObject
		}
		if full then
			res[:conquest] = @conquest.toObject
		else
			res[:score] = @conquest.score
		end
		res
	end
	def status()
		res = Hash.new
		if @room == "lobby" then
			res[:type] = Msg::USERSTAT_LOBBY
		else
			res[:data] = $rooms[@room].id
			if $rooms[@room].game[:activated] then
				res[:type] = Msg::USERSTAT_PLAYING
			else
				res[:type] = Msg::USERSTAT_WAIT
			end
		end
		res
	end
end