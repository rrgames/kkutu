var ExplDiv = document.createElement("div");
ExplDiv.id = "ExplDiv";
ExplDiv.style.display = "none";
ExplDiv.style.position = "absolute";
ExplDiv.style.maxWidth = "400px";
ExplDiv.style.border = "2px double #AAAAAA";
ExplDiv.style.padding = "5px";
ExplDiv.style.color = "#FFFFFF";
ExplDiv.style.backgroundColor = "rgba(0, 0, 0, 0.6)";
ExplDiv.style.pointerEvents = "none";
ExplDiv.style.zIndex = 7;

document.body.appendChild(ExplDiv);

window.addEventListener("mousemove", function(e){
	ExplDiv.style.left = Math.max(Math.min(e.clientX, window.innerWidth-ExplDiv.offsetWidth), 0)+"px";
	ExplDiv.style.top = Math.max(Math.min(e.clientY, window.innerHeight-ExplDiv.offsetHeight), 0)+"px";
});
function makeExpl(obj, expl){
	obj.addEventListener("mouseover", function(e){
		showExpl(expl);
	});
	obj.addEventListener("mouseout", function(e){
		showExpl();
	});
}
function showExpl(text){
	ExplDiv.style.display = text?"":"none";
	ExplDiv.innerHTML = text;
}