var wSocket;
WebSocket.prototype.sendObject = function(obj){
	this.send(JSON.stringify(obj));
}

function connectSocketServer(token){
	var url = "wss://rrgames.me:12999?token="+token;
	wSocket = new WebSocket(url);

	wSocket.onmessage = terminal;
	wSocket.onopen = function(e){
		//console.info("서버 연결 완료");
	}
	wSocket.onclose = function(e){
		switch(e.code){
			case 1006:
				alert("(14.11.08) 베타 테스트 기간이 끝났습니다!");
				break;
			case 3001:
				alert("다른 곳에서 해당 계정으로 연결해 연결이 종료되었습니다.");
				break;
			default:
				alert("서버와의 연결이 예기치 않게 종료되었습니다.\n종료 번호: "+e.code+"\n종료 이유: "+e.reason);
		}
	}
}
function terminal(e){
	var msg;
	if(e.constructor == Object){
		msg = e;
	}else{
		msg = JSON.parse(e.data);
	}
	var I;
	switch(msg.code){
		case WELCOME:
			my = new Client(msg.me.id, msg.me.name, msg.me);
			user = {};
			for(I in msg.clients){
				user[I] = new Client(msg.clients[I].id, msg.clients[I].name, {score: msg.clients[I].score, status: msg.clients[I].status});
			}
			room = {};
			for(I in msg.rooms){
				room[I] = new Room(msg.rooms[I]);
			}
			hiderMsg();
			updateRoom();
			updateMe();
			playBGM("lobby");
			requestFetch();
			break;
		case ERROR:
		case WARN:
			if(msg.text == "password"){
			}else{
				alert(WARN_MESSAGE[msg.text]);
			}
			break;
		case ALERT:
			alert(msg.text);
			break;
		case USER_CONNECT:
			user[msg.id] = new Client(msg.id, msg.name, {score: msg.data.score});
			drawUserList();
			break;
		case USER_DISCONNECT:
			delete user[msg.id];
			drawUserList();
			if(my.room){
				var ui = 0;
				while(my.room.user[ui]){
					if(my.room.user[ui] == msg.id){
						updateRoom();
						break;
					}
					ui++;
				}
			}
			break;
		case USER_UPDATE:
			for(I in msg.data){
				user[msg.data.id].data[I] = msg.data[I];
			}
			if(msg.data.id == my.id){
				my.data = msg.data;
				updateMe();
			}
			drawUserList();
			break;
		case NEWBIE:
			hiderMsg("Newbie");
			gn("NewbieNameText").value = msg.def;
			break;
		case CHAT:
		case SUDO_CHAT:
		case BLOCKED:
			attachChat(msg);
			break;
		case GET_STATUS:
			user[msg.data.id].data = msg.data;
			user[msg.data.id].data.status = msg.data.status;
			dialog("ST", msg.data.id);
			break;
		case ROOM_CREATE:
		case ENTER:
			if(my.id == msg.id || msg.code == ROOM_CREATE){
				my.room = new Room(msg);
				my.cm = my.id;
				enterRoom(msg.roomId);
			}else{
				if(user.hasOwnProperty(msg.id)){
					msg.data = user[msg.id].name;
					attachChat(msg);
				}else{
					console.warn("유저 정보 조회에 실패");
					setTimeout(wSocket.onmessage, 1000, e);
				}
			}
			break;
		case EXIT:
			if(my.id == msg.id){
				my.room = null;
				exitRoom();
				if(msg.kicked){
					alert("방장에 의해 강제 퇴장되었습니다.");
				}
			}else{
				msg.data = user[msg.id].name;
				attachChat(msg);
			}
			break;
		case ROOM_UPDATE:
			if(!room[msg.roomId]){
				if(msg.data.hasOwnProperty("id")){
					msg.data.roomId = msg.data.id;
				}
				room[msg.roomId] = new Room(msg.data);
			}else{
				var ro = room[msg.roomId];
				ro.title = msg.data.title;
				ro.game = msg.data.game;
				ro.master = msg.data.master;
				ro.user = msg.data.user;
				ro.config = msg.data.config;
			}
			var forced = false;
			if(msg.data.game){
				forced = msg.data.game.hasOwnProperty("conseq");
			}
			if(my.room != null){
				if(my.room.id == msg.roomId){
					my.room = room[msg.roomId];
					if(forced){
						my.room.conseq = msg.data.game.conseq;
					}
				}
			}
			updateRoom(forced);
			break;
		case ROOM_DELETE:
			delete room[msg.roomId];
			updateRoom();
			break;
		case READY:
			my.room.getUserById(msg.id).game.ready = msg.data;
			if(my.id == msg.id){
				my.ready = msg.data;
				ReadyBtn.value = my.ready?"준비 취소":"준비";
			}
			updateRoom();
			break;
		case START_PRACTICE:
			my.pSubj = msg.subj;
			my.pRound = 0;
		case START:
			my.practice = msg.code == START_PRACTICE;
			dialog();
			my.room.conseq = msg.conseq;
			startGame(my.practice);
			break;
		case ROUND_START:
			my.room.game = msg.data.game;
			startRound(msg.data.game.round);
			break;
		case TURN_START:
			startTurn(msg);
			break;
		case ROUND_END:
			drawTimeout();
			break;
		case GAME_UPDATE:
			updateGame(msg.data);
			break;
		case ACCEPT:
			my.pNext = msg.next;
			accept(msg.data);
			break;
		case WRONG_WORD:
			if(msg.hasOwnProperty("option")){
				wrong(BLOCKED_OPTION[msg.option]+": "+msg.data);
			}else{
				wrong(msg.data);
			}
			break;
		case NO_CHANCE:
			wrong("찬스 단어 없음");
			break;
		case AGAIN_WORD:
			wrong("이미 쓴 단어: "+msg.data);
			break;
		case FOUND_PLAYER:
			foundTick(10);
			gn("RG_Finding").innerHTML = "&nbsp;";
			gn("RG_FDText").innerHTML = "상대를 찾았습니다!";
			gn("RG_Found").style.display = "";
			break;
		case RANK_START:
			dialog();
			break;
		case FINISH:
			finishGame(msg.data);
			break;
		default:
			console.warn("알 수 없는 유형: ", msg);
	}
}

function send(data){
	var sd = {
		code: CHAT,
		data: data,
		item: -1,
		id: my.id
	};
	if(isGaming){
		if(g.turn == my.id){
			var il = g.item.length;
			for(var i=0; i<il; i++){
				// 현재 한 턴에 하나의 아이템만 사용할 수 있다.
				if(g.item[i] == 1){
					sd.item = i;
					break;
				}
			}
		}
	}
	wSοcket.sendObject(sd);
}
function requestNewbie(name){
	wSocket.sendObject({
		code: NEWBIE,
		name: name
	});
}
function requestCreateRoom(data){
	wSocket.sendObject({
		code: ROOM_CREATE,
		data: data,
		id: my.id
	});
}
function requestEnterRoom(e){
	var o = room[e.currentTarget.getAttribute("data-id")];
	var pw;
	if(room[o.id].config.password){
		pw = prompt("방 ["+room[o.id].title+"]에 입장하려면 암호를 입력하세요.");
	}else{
		pw = "";
	}
	if(pw == null) return;
	wSocket.sendObject({
		code: ENTER,
		data: {
			id: o.id,
			pw: pw
		},
		id: my.id
	});
}
function requestConfigRoom(){
	var q = {
		title: gn("RS_Title").value,
		password: gn("RS_Password").value,
		userLimit: gn("RS_UserLimit").value,
		item: gn("RS_Item").checked,
		rule: gn("RS_Rule").value,
		round: gn("RS_Round").value
	}
	if(q.rule == "KS" || q.rule == "KK"){
		q.option = {
			korean: gn("RS_Foreign").checked,
			substantive: gn("RS_Substantive").checked
		}
	}
	wSocket.sendObject({
		code: ROOM_UPDATE,
		data: q
	});
}
function requestStartRound(){
	if(my.practice){
		my.pM = false;
		my.pRound++;
		my.pCount = 0;
		if(my.pRound >= 5){
			GameSubjectPanel.innerHTML = "게임 종료!";
			setTimeout(closeGameBox, 2000);
			return;
		}
		my.pChain = new Array();
		terminal({code: ROUND_START, data: { game: {
			round: my.pRound,
			time: 150000
		}}});
		return;
	}
	if(!isGaming) return;
	wSocket.sendObject({
		code: ROUND_START,
		data: {
			roomId: my.room.id
		}
	});
}
function requestStartTurn(){
	if(my.practice){
		var bj = {code: TURN_START, 
			subj: my.pSubj[my.pRound],
			turn: my.id,
			time: 15000
		};
		var du = applyDueum(bj.subj, my.room.config.rule);
		if(du != null){
			bj.subj2 = du;
		}
		terminal(bj);
		return;
	}
	if(!isGaming) return;
	wSocket.sendObject({
		code: TURN_START,
		data: {
			roomId: my.room.id
		}
	});
}
function requestStart(){
	wSocket.sendObject({
		code: TRY_START,
		data: {
			roomId: my.room.id
		}
	});
}
function requestToggleReady(){
	wSocket.sendObject({
		code: READY,
		data: !my.ready,
		id: my.id
	});
}
function requestNextTurn(){
	if(my.practice){
		var q = g.recentPassed.charAt(g.recentPassed.length-1);
		if(my.room.config.rule == "KT"){
			q = g.recentPassed.substr(g.recentPassed.length-3, 2)+q;
		}
		var r = 10-Math.floor(g.ctt/15000);
		updateGameUser();
		var bj = {code: TURN_START,
			subj: q,
			time: 15000-1400*r
		}
		var du = applyDueum(bj.subj, my.room.config.rule);
		if(du != null){
			bj.subj2 = du;
		}
		if(my.pM){
			my.pM = false;
			bj.turn = my.id;
			terminal(bj);
		}else{
			my.pM = true;
			aDemagnify(my.pDiv);
			aMagnify(my.botDiv);
			bj.turn = "bot";
			terminal(bj);
			my.pTimer = setTimeout(function(){
				aDemagnify(my.botDiv);
				if(my.pNext){
					terminal({code: ACCEPT,
						data: {
							word: my.pNext.word,
							mean: my.pNext.mean,
							score: my.pNext.word.length*10,
							usage: 0,
							item: -1
						}
					});
				}
			}, 500+Math.round(Math.random()*1500));
		}
		return;
	}
	if(!isGaming) return;
	wSocket.sendObject({
		code: TURN_PLAY,
		data: {
			roomId: my.room.id
		}
	});
}
function requestTurnTimeout(){
	g.timer = -1;
	if(!isGaming) return;
	if(my.practice){
		terminal({code: ROUND_END});
		return;
	}
	wSocket.sendObject({
		code: TURN_TIMEOUT,
		data: {
			roomId: my.room.id
		}
	});
}
function requestFindPlayer(rule){
	my.recentRule = rule;
	wSocket.sendObject({
		code: FIND_PLAYER,
		rule: rule
	});
}
function requestCancelMatch(){
	wSocket.sendObject({
		code: CANCEL_MATCH
	});
}
function requestHandshake(){
	wSocket.sendObject({
		code: HANDSHAKE
	});
}
function requestKick(id){
	wSocket.sendObject({
		code: KICK,
		id: id
	});
}
function requestLoadStatus(id){
	wSocket.sendObject({
		code: GET_STATUS,
		id: id
	});
}
function requestPractice(){
	wSocket.sendObject({
		code: START_PRACTICE,
		rule: my.room.config.rule
	});
}
function requestEndGame(){
	wSocket.sendObject({
		code: TRY_END,
		data: {
			roomId: my.room.id
		}
	});
}
function requestExitRoom(){
	wSocket.sendObject({
		code: EXIT
	});
}