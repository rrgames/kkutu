TOTAL_TIME = 120000

module Con

	SUDO_LIST = ["460989154041547", "699073610173937"]
	
	C_A = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
	C_AL = 26
	C_C = ["b","c","d","f","g","h","l","m","n","p","r","s","t","v"]
	C_CL = 14
	C_V = ["a","e","e","e","i","o","u"]
	C_VL = 7
	C_J = ["あ","い","う","え","お","か","き","く","け","こ","さ","し","す","せ","そ","た","ち","つ","て","と","な","に","ぬ","ね","の","は","ひ","ふ","へ","ほ","ま","み","む","め","も","や","ゆ","よ","ら","り","る","れ","ろ"]
	C_JL = 43
	HTML_L = {
		"<" => "&lt;",
		">" => "&gt;",
		"&" => "&amp;"
	}
	NUM_RULES = 5
	RULE_INDEX = {
		"KT" => 0,
		"ES" => 1,
		"KS" => 2,
		"KK" => 3,
		"JS" => 4
	}
	RULE_TABLE = {
		"KT" => "voca_ENG",
		"ES" => "voca_ENG",
		"KS" => "voca_KOR",
		"KK" => "voca_KOR",
		"JS" => "voca_JAP"
	}
	RULE_KT = "KT"
	RULE_ES = "ES"
	RULE_KS = "KS"
	RULE_KK = "KK"
	RULE_JS = "JS"

	# [0__] 시스템
	WELCOME = "001"
	ERROR = "002" # 서버의 오류
	ALERT = "003"

	# [1__] 통신
	USER_CONNECT = "101"
	USER_DISCONNECT = "102"
	NEWBIE = "103"
	USER_UPDATE = "104"
	
	# [2__] 채팅
	CHAT = "201"
	BLOCKED = "202"
	GET_STATUS = "203"
	
	# [3__] 방
	ENTER = "301"
	EXIT = "302"
	ROOM_CREATE = "303"
	ROOM_DELETE = "304"
	ROOM_UPDATE = "305"
	
	# [4__] 게임
	READY = "401"
	TRY_START = "402"
	START = "403"
	WARN = "404" # 클라이언트의 오류
	GAME_UPDATE = "405"
	FINISH = "406"
	TRY_END = "407"
	KICK = "408"
	START_PRACTICE = "409"
	ROUND_START = "411"
	ROUND_END = "412"
	TURN_START = "421"
	TURN_TIMEOUT = "422"
	TURN_PLAY = "423"
	ACCEPT = "424"
	WRONG_WORD = "425"
	NO_CHANCE = "426"
	AGAIN_WORD = "427"
	FIND_PLAYER = "431"
	FOUND_PLAYER = "432"
	CANCEL_MATCH = "433"
	HANDSHAKE = "434"
	RANK_START = "435"
	
	# [5__] 관리자 명령
	SUDO_CHAT = "501"
	
	# 차단된 상태에서도 보낼 수 있는 요청
	BLOCK_THROUGH = [
		Con::ROUND_START,
		Con::TURN_START,
		Con::TURN_PLAY,
		Con::TURN_TIMEOUT,
		Con::TRY_END,
		Con::EXIT
	]

end
module GameType
	
	NORMAL = "normal"
	RANKED = "ranked"
	
end
module Korean
	
	RIEUL_TO_NIEUN = [4449, 4450, 4452, 4453, 4454, 4457, 4458, 4459, 4460, 4462, 4463, 4464, 4465, 4467, 4468]
	NIEUN_TO_IEUNG = [4455, 4456, 4461, 4466, 4469]
	
end
module Msg
	
	NEWBIE_NAME_LENGTH = "newbieNameLength"
	NEWBIE_NAME_CHAR = "newbieNameChar"
	NEWBIE_NAME_OCCUPIED = "newbieNameOccupied"
	ALREADY_ACTIVATED = "alreadyActivated"
	NULL_ROOM = "nullRoom"
	FULL_ROOM = "fullRoom"
	HAVE_BEEN_KICKED = "haveBeenKicked"
	EMPTY_TITLE = "emptyTitle"
	WRONG_PASSWORD = "wrongPassword"
	NOT_YET_READIED = "notYetReadied"
	NOT_ENOUGH_PLAYER = "notEnoughPlayer"
	
	USERSTAT_LOBBY = "lobby"
	USERSTAT_WAIT = "wait"
	USERSTAT_PLAYING = "playing"
	
end