$terminal = {
	"qKT" => Array.new,
	"qES" => Array.new,
	"qKS" => Array.new,
	"qKK" => Array.new,
	"qJS" => Array.new
}
$matchThread = Thread.new do
	loop do
		# 4초마다 매칭을 시킨다.
		# 큐는 항상 :rating에 대해 내림차순 정렬되어 있다.
		$terminal.each do |key, value|
			dl = Array.new
			value.each_index do |index|
				c = value[index]
				d = value[index+1]
				if d == nil then
					if c[:range] < 200 then
						c[:range] += 3
					end
				else
					if c[:rating]-d[:rating] <= [c[:range], d[:range]].min then
						# 매칭 성공, 서로 단추를 누르면 게임을 시작할 수 있다.
						# 이 때 큐에서 빠진다
						send(c[:client].socket, {
							:code => Con::FOUND_PLAYER
						})
						send(d[:client].socket, {
							:code => Con::FOUND_PLAYER
						})
						c[:client].rival = d[:client].id
						d[:client].rival = c[:client].id
						dl.push(c[:client].id, d[:client].id)
					elsif c[:range] < 200 then
						c[:range] += 4
					end
				end
			end
			value.delete_if do |item|
				dl.include?(item[:client].id)
			end
		end
		sleep 4
	end
end
def addRankQueue(client, rule)
	ca = $terminal["q#{rule}"]
	r = {
		:client => client,
		:rating => client.conquest.rating[Con::RULE_INDEX[rule]],
		:range => 0
	}
	i = ca.index do |item|
		item[:rating] > r[:rating]
	end
	if i == nil then
		i = 0
	end
	ca.insert(i, r)
	client.terminal = rule
	# 여기서 MMR 순으로 정렬
end