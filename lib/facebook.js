window.fbAsyncInit = function() {
	Hider.style.display = "none";
	FB.init({
		appId      : '1479728205638536',
		xfbml      : true,
		version    : 'v2.1'
	});
	requestFBStatus();
};
setTimeout(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/ko_KR/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}, 200, document, 'script', 'facebook-jssdk');
setTimeout(function(){
	if(Hider.style.display != "none"){
		window.fbAsyncInit();
	}
}, 1000);

function requestFBStatus(){
	FB.getLoginStatus(function(res){
		switch(res.status){
			case "connected":
				connectSocketServer(res.authResponse.accessToken);
				hiderMsg();
				break;
			case "unknown":
			case "not_authorized":
				hiderMsg("FBLogin");
				break;
			default:
				console.warn("알 수 없는 유형: ", res.status);
		}
	});
}
function requestFetch(){
	FB.api("/me", "GET", null, function(res){
		my.fb = res;
	});
}
function requestFBLogin(){
	FB.login(function(res){
		requestFBStatus();
	}, {scope: "publish_actions"});
}
function requestShareGameResult(){
	var text = my.fb.name+"님이 "+RULE_NAME[my.room.config.rule]+" 경기에서 "+g.result.score+"점을 획득";
	if(g.result.rank == 0){
		text += "하고 1등을 차지했습니다!";
	}else{
		text += "했습니다!";
	}
	FB.api("/me/feed", "POST", {
		message: text,
		link: "http://14.63.167.204:3001/kkutu/"
	}, function(res){
		console.info(res);
		if(!res.hasOwnProperty("error")){
			ResultShareBtn.disabled = true;
			ResultShareBtn.value = "공유 완료";
		}
	});
}