var version = "0.9.1.2";

function showDevelop(){
	var s = "이 서버는 테스트 서버입니다.\n개발자의 원활한 개발을 위해 본 서버와는 별개로 만들어진 서버이며, 예기치 않게 서버와의 연결이 종료될 수 있습니다.";
	alert(s+="\n\n끄투 테스트 서버\n버전: "+version);
}

var my = new Object();
var fbToken;
var user = new Object();
var room = new Object();

String.prototype.getByteLength = function(b,i,c){
	for(b=i=0; c=this.charCodeAt(i++); b+=c>>11?2:c>>7?2:1);
	return b;
}
function sum(arr){
	var r=0; arr.forEach(function(o,i,m){r+=o;});
	return r;
}
function max(arr){
	var r=Number.MIN_VALUE; arr.forEach(function(o,i,m){r=Math.max(r,o);});
	return r;
}
function gi(name){
	return document.getElementById(name);
}
function gn(name){
	return document.getElementsByName(name)[0];
}
function toFixedFour(num){
	var R = num.toString();
	R = "0000".substr(0, 4-R.length)+R;
	return R;
}
function newbieNameTextChanged(){
	var obj = gn("NewbieNameText");
	gn("NewbieNameByte").innerHTML = obj.value.getByteLength()+"/16";
}
function attachChat(data){
	ChatBox.appendChild(new ChatDiv(data).div);
	ChatBox.scrollTop = ChatBox.scrollHeight;
}
function drawUserList(){
	if(my.room) return;
	var uc = 0;
	UserBox.innerHTML = "";
	for(var I in user){
		if(user[I].name == null){
			console.warn("null 계정 감지");
			continue;
		}
		uc++;
		UserBox.appendChild(new UserDiv(user[I]).div);
	}
	TopStatus.innerHTML = "<font color='#000000'>"+uc+"</font><label style='font-size: 13px;'>명 접속</font>";
}
function drawRoomUserList(){
	RoomUserBox.innerHTML = "";
	for(var I in my.room.user){
		RoomUserBox.appendChild(new RoomUserDiv(my.room.user[I], my.room, I).div);
	}
}
function enterRoom(id){
	RoomBox.style.display = "none";
	LobbyMenu.style.display = "none";
	RoomMenu.style.display = "";
	MyRoom.style.display = "";
	ChatBox.innerHTML = "";
	ReadyBtn.value = "준비";
	TopStatus.innerHTML = "방에 들어가는 중";
	dialog();
	if(room[id])	my.cm = room[id].master;
	my.ready = false;
}
function exitRoom(){
	if(g){
		clearInterval(g.ef);
		clearInterval(g.timer);
		isGaming = false;
	}
	GameBox.style.display = "none";
	GameMenu.style.display = "none";
	RoomBox.style.display = "";
	dialog();
	MyRoom.style.display = "none";
	RoomMenu.style.display = "none";
	LobbyMenu.style.display = "";
	ChatBox.innerHTML = "";
	TopStatus.innerHTML = "대기실";
	drawUserList();
	updateMe();
}
function updateRoom(forceUpdate){
	RoomTable.innerHTML = "";
	for(var I in room){
		RoomTable.appendChild(new RoomDiv(room[I], requestEnterRoom).div);
	}
	if(my.room != null){
		if(my.cm != room[my.room.id].master){
			my.cm = room[my.room.id].master;
			attachChat({
				code: MASTER_CHANGED,
				masterId: my.cm
			});
		}
		TopStatus.innerHTML = "[방&nbsp;<font color='#000000'>"+my.room.title+"</font>]";
		var master = my.room.master == my.id;
		StartBtn.style.display = master?"":"none";
		RSBtn.style.display = StartBtn.style.display;
		ReadyBtn.style.display = master?"none":"";
		if(isGaming){
			if(forceUpdate) updateGameUser();
		}else{
			drawRoomUserList();
		}
		RoomConfigBox.innerHTML = getConfigText(my.room.config);
	}
}
function updateMe(){
	MyName.innerHTML = my.name;
	my.level = getLevel(my.data.conquest.score);
	my.goalScore = getGoalScore(my.level);
	my.beforeScore = getGoalScore(my.level-1);
	MyBadge.src = "./image/level/lv"+toFixedFour(my.level)+".png";
	MyLevel.innerHTML = "<label style='font-size: 12px;'>레벨</label>&nbsp;"+my.level;
	MyExp.style.width = Math.round((my.data.conquest.score-my.beforeScore)/(my.goalScore-my.beforeScore)*106)+"px";
	var normalWin = sum(my.data.conquest.normalWin);
	var ratingWin = sum(my.data.conquest.rankedWin);
	var rc = "통산 <label style='font-size: 16px;'>"+prettyNumber(normalWin+ratingWin)+"</label>승";
	MyRecord.innerHTML = rc;
	var ec = "<img src='./image/rank.png' align='middle' /> "+prettyNumber(max(my.data.conquest.rating).toFixed(1))+"<br />";
	ec += "<img src='./image/ping.png' align='middle' /> "+prettyNumber(my.data.conquest.ping);
	MyETC.innerHTML = ec;
}
function acceptCreateRoom(){
	var q = {
		title: gn("RS_Title").value,
		password: gn("RS_Password").value,
		userLimit: gn("RS_UserLimit").value,
		item: gn("RS_Item").checked,
		rule: gn("RS_Rule").value,
		round: gn("RS_Round").value
	}
	if(q.rule == "KS" || q.rule == "KK"){
		q.option = {
			korean: gn("RS_Foreign").checked,
			substantive: gn("RS_Substantive").checked
		}
	}
	requestCreateRoom(q);
	dialog();
}
function getConfigText(config){
	var R = "<div class='RoomConfigItem' style='width: 150px;'>"+RULE_NAME[config.rule]+"</div>"; 
	R += "<div class='RoomConfigItem' style='width: 100px;'>"+(config.item?"<font color='#0000FF'>아이템전</font>":"<font color='#666666'>노템전</font>")+"</div>";
	R += "<div class='RoomConfigItem' style='width: 100px; color: #007700;'>"+config.round+"<label class='RoomConfigText'>&nbsp;라운드</label></div>";
	R += "<div class='RoomConfigItem' style='width: 100px;'><label class='RoomConfigText'>인원&nbsp;</label>"+my.room.user.length+"/"+config.userLimit+"</div>";
	return R;
}
function dialog(type, data){
	if(Dialog.getAttribute("data-type") == "RG"){
		if(type) return;
	}
	Dialog.setAttribute("data-type", type);
	if(!type){
		Dialog.style.display = "none";
		return;
	}
	if(!data) data = new Object();
	Dialog.style.display = "";
	var v = document.getElementById("DG_"+type);
	if(!v) return;
	var s = [v.getAttribute("data-width"), v.getAttribute("data-height")];
	DialogTitle.innerHTML = data.title?data.title:v.getAttribute("data-title");
	DialogStage.innerHTML = v.innerHTML;
	Dialog.style.width = s[0]+"px";
	Dialog.style.height = s[1]+"px";
	Dialog.style.top = "calc(50% - "+Math.round(s[1]*0.5)+"px)";
	Dialog.style.left = "calc(50% - "+Math.round(s[0]*0.5)+"px)";
	if(type == "RS"){
		if(data.title){
			gn("RS_Title").value = my.name+"님의 방";
		}else{
			gn("RS_Title").value = my.room.title;
			gn("RS_Password").value = my.room.config.password?"＊＊＊＊":"";
			gn("RS_UserLimit").value = my.room.config.userLimit;
			gn("RS_Item").checked = my.room.config.item;
			gn("RS_Rule").selectedIndex = RULE_INDEX.indexOf(my.room.config.rule);
			gn("RS_Round").value = my.room.config.round;
			gn("RS_Optional").innerHTML = RS_OPTIONAL[gn("RS_Rule").value];
			if(my.room.config.rule == "KS" || my.room.config.rule == "KK"){
				gn("RS_Foreign").checked = my.room.config.option.korean;
				gn("RS_Substantive").checked = my.room.config.option.substantive;
			}
		}
	}
	if(type == "ST"){
		var obj = user[data].data;
		my.recentStatus = data;
		if(data == my.id){
			obj = my.data;
		}
		if(obj.hasOwnProperty("conquest")){
			obj.level = getLevel(obj.conquest.score);
			gn("ST_RankImg").src = "./image/level/lv"+toFixedFour(obj.level)+".png";
			gn("ST_Name").innerHTML = obj.name;
			gn("ST_Level").innerHTML = "레벨 "+obj.level;
			var gs = getGoalScore(obj.level);
			var pgs = getGoalScore(obj.level-1);
			makeExpl(gn("ST_Level"), "<label style='font-size: 14px;'>경험치: "+prettyNumber(Math.round(obj.conquest.score))+" / "+prettyNumber(gs)+"</label>");
			gn("ST_Exp").style.width = Math.round((obj.conquest.score-pgs)/(gs-pgs)*200)+"px";
			obj.normalWin = sum(obj.conquest.normalWin);
			obj.rankedWin = sum(obj.conquest.rankedWin);
			gn("ST_Record").innerHTML = ("통산 "+(obj.normalWin+obj.rankedWin)+"승")+((obj.rankedWin > 0)?"<label style='color: #666666; font-size: 12px;'>&nbsp;(일반 "+obj.normalWin+"+순위 "+obj.rankedWin+")</label>":"");
			RULE_INDEX.forEach(function(item, index, my){
				gn("ST_Normal"+item).innerHTML = obj.conquest.normalPlay[index]+"전 "+obj.conquest.normalWin[index]+"승";
				gn("ST_Ranked"+item).innerHTML = obj.conquest.rankedPlay[index]+"전 "+obj.conquest.rankedWin[index]+"승";
				gn("ST_Rating"+item).innerHTML = obj.conquest.rating[index];
			});
			var r = obj.name+"님은 현재 ";
			switch(obj.status.type){
				case USERSTAT_LOBBY:
					r += "<font color='#222277'>로비</font>에 있습니다.";
					break;
				case USERSTAT_WAIT:
					r += "<font color='#227722'>"+obj.status.data+"번 방</font>에서 대기하고 있습니다.";
					break;
				case USERSTAT_PLAYING:
					r += "<font color='#777722'>"+obj.status.data+"번 방</font>에서 게임을 하고 있습니다.";
					break;
				default:
			}
			gn("ST_Position").innerHTML = r;
			gn("ST_Description").innerHTML = obj.conquest.description;
		}else{
			requestLoadStatus(data);
			dialog("STL");
		}
	}
	if(data.onAccept){
		gn("RS_OKBtn").setAttribute("onclick", data.onAccept);
	}
}
function hiderMsg(type){
	if(!type){
		Hider.style.display = "none";
		return;
	}
	Hider.style.display = "";
	HiderStage.innerHTML = this["HD_"+type].innerHTML;
}