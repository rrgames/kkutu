require 'json'

class Room
	attr_accessor :id
	attr_accessor :title
	attr_accessor :config
	attr_accessor :master
	attr_accessor :users
	attr_accessor :game
	attr_accessor :timer
	attr_accessor :blackList
	# config <Hash> 속성
	#	:userLimit	제한 인원
	#	:password	방 암호
	#	:item			아이템 사용 가능 여부
	#	:rule				게임 방식
	#	:rating			순위 경쟁 여부
	#	:round			판 라운드 수
	#	:option			게임 추가 방식
	#		:korean	외래어 금지
	
	# game <Hash> 속성
	#	:type				게임 유형 (const.rb 참고)
	#	:activated			게임 진행 여부
	#	:round				진행 판
	#	:conseq			게임 턴 (Array, 유저)
	#	:turn				현재 턴 인덱스
	#	:time				판 제한 시간
	#	:subj				제시어
	#	:tSubj				전체 제시어
	#	:wordList			사용 단어 내역 (한 판에서)
	#	:chain				사용 단어 내역 (한 라운드에서)
	#	:recentWord	최근 사용 단어
	#	:parsing			사전 파싱 중 여부
	#	:tTime				턴 제한 시간
	#	:ttt					사용자가 파싱 요청을 한 시점의 시간
	#	:leftSent			타임아웃 처리를 위해 받아야 할 요청 수
	#	:dir					턴 진행 방향 (1이 오른쪽)
	#	:dueum			두음법칙으로 대체 사용 가능한 제시어 (없으면 nil)
	
	def initialize()
		@id = $rid
		$rid = $rid+1
		if $rid > 999 then
			$rid = 100
		end
		@users = Array.new
		@config = {
			:userLimit => 6,
			:password => "",
			:item => true,
			:rule => Con::RULE_KT,
			:rating => false,
			:round => 5,
			:option => {
				:korean => false,
				:substantive => false
			}
		}
		@game = {
			:activated => false
		}
		@blackList = Array.new
	end
	def create(masterId)
		@master = masterId
		#$dbObj.query("INSERT INTO `room` (id) VALUES(#{@id});")
	end
	def remove()
		#$dbObj.query("DELETE FROM `room` WHERE `id` = #{@id};")
	end
	def publishData(code = Con::ROOM_UPDATE, fullObject = false)
		publish("global", {
			:code => code,
			:roomId => @id,
			:data => toObject(fullObject)
		})
	end
	def publishGameData(code = Con::GAME_UPDATE)
		ul = Array.new
		@users.each do |item|
			ul.push(item.toObject)
		end
		publish(@id, {
			:code => code,
			:roomId => @id,
			:data => {
				:user => ul,
				:game => @game
			}
		})
	end
	def getUserById(id)
		r = nil
		@users.each do |item|
			if item.id == id then
				r = item
				break
			end
		end
		r
	end
	def ready()
		r = true
		@users.each do |item|
			if item.id != @master then
				if item.game.ready != true then
					r = false
					break
				end
			end
		end
		r
	end
	def startGame(type = GameType::NORMAL)
		@game[:type] = type
		@game[:activated] = true
		@game[:round] = 0
		@game[:turn] = 0
		@game[:dir] = 1
		@game[:wordList] = Hash.new
		sq = @users.shuffle
		@game[:conseq] = Array.new
		sq.each do |item|
			@game[:conseq].push(item.id)
		end
		@game[:tSubj] = getRandomSubj(@config[:rule], @config[:round])
		@users.each do |item|
			item.game.start()
		end
	end
	def endGame()
		# 순위 매기기
		rank = @users.sort do |a, b|
			b.game.score-a.game.score
		end
		ri = 1
		# 보상 지급 (game.rb의 flush 참고)
		ra = Array.new
		rank.each do |item|
			tie = false
			if ri > 1 then
				if item.game.score == ra[ri-2][:score] then
					ri -= 1
					tie = true
				end
			end
			ra.push(item.game.flush(ri))
			ri += 1
			if tie then
				ri += 1
			end
		end
		if @game[:type] == GameType::NORMAL then
			rank[0].conquest.normalWin[Con::RULE_INDEX[@config[:rule]]] += 1
		end
		if @game[:type] == GameType::RANKED then
			rank[0].conquest.rankedWin[Con::RULE_INDEX[@config[:rule]]] += 1
		end
		# 사용한 단어 집계 및 데이터베이스에 올리기
		# Array에 모아서 보내려 했지만 실패...
		# key 가장 앞에 _가 있으면 난이도를 변경해야 한다.
		@game[:wordList].each do |key, value|
			if key[0] == "_" then
				$dbObj.query("UPDATE `#{Con::RULE_TABLE[@config[:rule]]}` SET `difficulty_#{@config[:rule]}` = `difficulty_#{@config[:rule]}` + #{value} WHERE `word` = '#{key[1..-1]}';")
			else
				$wordsHit[@config[:rule]] += value
				$dbObj.query("UPDATE `#{Con::RULE_TABLE[@config[:rule]]}` SET `count_#{@config[:rule]}` = `count_#{@config[:rule]}` + #{value} WHERE `word` = '#{key}';")
			end
		end
		$dbObj.query("UPDATE `global` SET `value` = #{$wordsHit[@config[:rule]]} WHERE `name` = 'wordsHit_#{@config[:rule]}';")

		publish(@id, {
			:code => Con::FINISH,
			:data => ra,
			:wordList => @game[:wordList]
		})
		# 결과창을 띄운 뒤 잠시 후 방장이 activated를 false로 한다
	end
	def startRound()
	# 여기서 게임이 끝났는지 판단
		@game[:round] += 1
		@game[:chain] = Array.new
		@game[:recentWord] = nil
		if @game[:round] > @config[:round] then
			endGame()
		else
			@game[:time] = TOTAL_TIME
			@game[:subj] = @game[:tSubj][@game[:round]-1]
			publishGameData(Con::ROUND_START)
		end
	end
	def endRound(with)
		with.game.boom()
		if @game[:recentWord] then
			# 단어 난이도 증가
			wt = "_#{@game[:recentWord]}"
			if @game[:wordList].key?(wt) then
				@game[:wordList][wt] += 1
			else
				@game[:wordList][wt] = 1
			end
		end
		@game[:ttt] = 0
		endTurn(nil, -1)
		publish(@id, {
			:code => Con::ROUND_END
		})
	end
	def startTurn()
	# 턴 시작 및 턴 시간 초과를 담당
		time = getTurnTime(@game[:time])
		@game[:leftSent] = 0
		@game[:tTime] = time
		@users.each do |item|
			item.game.toSent = false
			@game[:leftSent] += 1
		end
		@game[:parsing] = false
		rv = {
			:code => Con::TURN_START,
			:turn => @game[:conseq][@game[:turn]],
			:subj => @game[:subj],
			:time => time
		}
		@game[:dueum] = applyDueum(@game[:subj][-1], @config[:rule])
		if @game[:dueum] then
			rv[:subj2] = @game[:dueum]
		end
		publish(@id, rv)
		@timer = now
	end
	def sendTimeout(from)
		if @timer == nil then
			return
		end
		if from.game.toSent == false then
			td = now - @timer
			# 타임아웃 요청이 오면 그 진위성 판단
			if td >= @game[:tTime] then
				if @game[:parsing] == true then
					# 사전 파싱 요청은 했는데 파싱 결과가 지연될 경우
					from.socket.send_text(JSON.generate({
						:code => Con::TURN_START,
						:turn => @game[:turn],
						:subj => @game[:subj],
						:time => 1000
					}))
				else
					from.game.toSent = true
					@game[:leftSent] -= 1
				end
			else
				# 타임아웃이 유효하지 않은 경우
				from.socket.send_text(JSON.generate({
					:code => Con::TURN_START,
					:turn => @game[:turn],
					:subj => @game[:subj],
					:time => td
				}))
			end
		end
		# 현재 턴인 사람을 제외하기 때문에 0이 아니라 1
		if @game[:leftSent] == 1 then
			endRound($clients[@game[:conseq][@game[:turn]]])
		end
	end
	def endTurn(with, item)
		dt = @game[:ttt] - @timer
		# 변화량에서 0.2초를 더 뺀다: 매우 빠르게 릴레이하면 게임이 끝나는 데 매우 오래 걸림
		@game[:time] -= dt + 200
		if with != nil then
			if item == -1 then
				if @config[:item] then
					if dt <= @game[:tTime]*0.3 then
						# 아이템 지급 - 제한 시간을 70% 이상 남기고 턴을 넘겼을 때
						$clients[@game[:conseq][@game[:turn]]].game.getItem(dt%4)
					end
				end
			else
				$clients[@game[:conseq][@game[:turn]]].game.useItem(item)
				case item
					when 1
						playTurn(@game[:dir], false)
					when 2
						@game[:dir] *= -1
					when 3
						playTurn(-1*@game[:dir], false)
				end
			end
			@game[:chain].push(with)
			if @game[:wordList][with] == nil then
				@game[:wordList][with] = 1
			else
				@game[:wordList][with] += 1
			end
			@game[:recentWord] = with
			case @config[:rule]
				when Con::RULE_KT
					@game[:subj] = with[-3, 3]
				when Con::RULE_ES, Con::RULE_KS, Con::RULE_KK
					@game[:subj] = with[-1]
				when Con::RULE_JS
					if with[-1] == "ー" then
						@game[:subj] = with[-2]
					else
						@game[:subj] = with[-1]
					end
			end
		end
		@timer = nil
		publishGameData()
	end
	def playTurn(dir = @game[:dir], andStart = true)
		@game[:turn] += dir
		if @game[:turn] >= @users.length then
			@game[:turn] = 0
		end
		if @game[:turn] < 0 then
			@game[:turn] = @users.length-1
		end
		if andStart then
			startTurn()
		end
	end
	def toObject(full = false)
		ul = Array.new
		@users.each do |item|
			ul.push(item.toObject(false))
		end
		r = {
			:id => @id,
			:title => @title,
			:master => @master,
			:user => ul,
			:config => {
				:userLimit => @config[:userLimit],
				:password => @config[:password].length > 0,
				:item => @config[:item],
				:rule => @config[:rule],
				:rating => @config[:rating],
				:round => @config[:round],
				:option => @config[:option]
			}
		}
		if @game[:activated] then
			r[:game] = {
				:activated => true,
				:round => @game[:round],
				:turn => @game[:turn],
				:time => @game[:time],
				:subj => @game[:subj]
			}
			if full then
				r[:game][:conseq] = @game[:conseq]
			end
		end
		r
	end
end