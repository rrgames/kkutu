function classify(obj){
	/* 쓰는 법
	obj
	└ _ARG:Array @생성자
	*/
	var R = obj._ARG;
	delete obj._ARG;
	for(var I in obj){
		R.prototype[I] = obj[I];
	}
	return R;
}

var Client = classify({
	_ARG: function(id, name, data){
		this.id = id;
		this.name = name;
		this.data = data;
		this.fb = null; // 자기만 이 속성을 가진다.
	},

	id: null,
	name: null,
	data: null,
	fb: null,
	ready: false,
	
	getLevel: function(){
		
	}
});
var Room = classify({
	_ARG: function(data){
		this.id = data.roomId || data.id;
		this.title = data.title;
		this.master = data.master;
		this.user = data.user;
		this.config = data.config;
		this.game = data.game;
	},

	id: null,
	title: null,
	master: null,
	user: null,
	config: null,
	game: null,

	getUserById: function(id){
		if(!this.user) return;
		var R;
		var l = this.user.length;
		for(var i=0; i<l; i++){
			if(this.user[i].id == id){
				R = this.user[i];
				break;
			}
		}
		return R;
	}
});
var UserDiv = classify({
	_ARG: function(data){
		this.data = data;
		this.div = document.createElement("div");
		this.div.id = "UserItem_"+data.id;
		this.div.className = "UserItem";
		this.validateDiv();
	},

	data: null,
	div: null,

	getExpl: function(lv){
		var R = "<font color='#FFFF00;'>레벨 "+lv+"</font>";
		return R;
	},
	validateDiv: function(){
		this.div.onclick = function(e){
			dialog("ST", e.currentTarget.id.slice(9));
		}
		var R = "";
		var sc = this.data.data.conquest?this.data.data.conquest.score:this.data.data.score;
		var lv = getLevel(sc);
		R += "<img src='./image/level/lv"+toFixedFour(lv)+".png' class='UserRank' />";
		R += "<label class='ChatUserName'>"+this.data.name+"</label>";
		makeExpl(this.div, this.getExpl(lv));
		this.div.innerHTML = R;
	}
});
var RoomUserDiv = classify({
	_ARG: function(data, room, index){
		this.room = room;
		this.data = data;
		this.index = index;
		this.div = document.createElement("div");
		this.div.className = "RoomUserItem";
		this.validateDiv();
	},

	room: null,
	data: null,
	div: null,
	index: null,

	validateDiv: function(){
		var R = "";
		R += "<div class='RoomUserImage'>"
			R += "<div style='border-bottom: 1px dashed #CCCCCC; height: 20px; background-color: rgba(0, 0, 0, 0.04);'>"
				R += "<div style='padding-top: 3px; float: left; width: 50px; text-align: left;'>&nbsp;&nbsp;"+(Number(this.index)+1)+"</div>";
				if(my.room.master == my.id){
					if(this.data.id != my.id){
						R += "<div class='KickBtn' onClick='requestKick(\""+this.data.id+"\");'>X</div>";
					}
				}
			R += "</div>";
		R += "</div>";
		R += "<div class='RoomUserName'>";
		R += "<img src='./image/level/lv"+toFixedFour(getLevel(this.data.score))+".png' class='UserRank' />";
		R += this.data.name+"</div>";
		R += "<div class='RoomUserInfo'>";
		if(this.data.id == this.room.master){
			R += "<label style='color: #0000FF'>방장</label>";
		}else{
			if(this.data.game.ready){
				R += "<label style='color: #007700'>준비 완료!</label>";
			}else{
				R += "<label style='color: #888888'>준비 중…</label>";
			}
		}
		R += "</div>";
		this.div.innerHTML = R;
	}
});
var GameUserDiv = classify({
	_ARG: function(data, room){
		this.room = room;
		this.data = data;
		this.div = document.createElement("div");
		this.div.className = "GameUserItem";
		this.validateDiv();
	},

	room: null,
	data: null,
	div: null,

	validateDiv: function(){
		this.div.id = "GameUser_"+this.data.id;
		if(this.data.id == "bot"){
			this.div.innerHTML = "<div class='GameUserImage'></div><div class='GameUserName'>끄투 로봇</div>";
			return;
		}
		try{
			user[this.data.id].item = this.data.game.item;
		}catch(e){
			return;
		}
		var R = "<div class='GameUserImage'></div>";
		R += "<div class='GameUserName'>";
		R += "<img src='./image/level/lv"+toFixedFour(getLevel(this.data.score))+".png' class='UserRank' />";
		R += this.data.name+"</div>";
		if(this.data.game.score != null){
			var sv = this.data.game.score.toString();
			if(sv.charAt() == "-"){
				sv = "-00".substr(0, 5-sv.length)+sv.slice(1);
			}else{
				sv = "000".substr(0, 4-sv.length)+sv;
			}
			sv = sv.split("");
			sv.forEach(function(item, index, my){
				R += "<div class='GameUserScore'>"+item+"</div>";
			});
		}
		this.div.innerHTML = R;
	}
});
var ScoreDiv = classify({
	_ARG: function(data){
		this.data = data;
		this.alpha = 1;
		this.left = 0;
		this.top = 0;
		this.font = 28;
		this.dF = 3;
		this.div = document.createElement("div");
		this.div.className = "ScoreItem";
		this.validateDiv();
	},
	
	left: null,
	top: null,
	font: null,
	dF: null,
	alpha: null,
	data: null,
	div: null,
	
	validateDiv: function(){
		this.div.style.left = this.left+"px";
		this.div.style.top = this.top+"px";
		this.div.style.fontSize = Math.round(this.font)+"px";
		this.div.style.opacity = this.alpha;
		
		var vs = this.data.word?(10*this.data.word.length):0;
		var R;
		if(this.data.score < 0){
			R = "<label style='color: #FF6666'>"+this.data.score+"</label>";
		}else{
			R = "+"+this.data.score;
			/*if(this.data.score != vs){
				R += "<br /><label style='font-size: 16px; color: #CCCCCC;'>보너스 +</label>"+(this.data.score-vs);
			}*/
		}
		this.div.innerHTML = R;
	}
});
var ScoreParticle = classify({
	_ARG: function(type, x, y, gX, gY, dV){
		this.div = document.createElement("div");
		this.div.className = "ScoreParticle";
		this.type = type;
		this.x = x;
		this.y = y;
		this.alpha = 1;
		this.gX = gX;
		this.gY = gY;
		this.dmX = dV || -(Math.random()*0.3);
		this.dmY = dV || -(Math.random()*0.3);
		this.width = 3+Math.random()*7;
		this.height = this.width;
		this.validateDiv();
		aParticle(this);
	},
	
	type: null,
	x: null,
	y: null,
	alpha: null,
	width: null,
	height: null,
	gX: null,
	gY: null,
	dV: null,
	div: null,
	
	validateDiv: function(){
		var s = this.div.style;
		s.top = Math.round(this.y)+"px";
		s.left = Math.round(this.x)+"px";
		s.opacity = this.alpha;
		s.width = Math.round(this.width)+"px";
		s.height = Math.round(this.height)+"px";
		switch(this.type){
			case "N":
				s.backgroundColor = "#FFFF77";
				break;
			case "C":
				s.backgroundColor = "#4444FF";
				break;
			default:
		}
	}
});
var RoomDiv = classify({
	_ARG: function(data, clickHandler){
		this.data = data;
		this.div = document.createElement("div");
		this.div.className = "RoomItem";
		this.div.setAttribute("data-id", data.id);
		this.div.onclick = clickHandler;
		this.validateDiv();
	},

	data: null,
	div: null,

	validateDiv: function(){
		var bgc = 0;
		if(this.data.game){
			if(this.data.game.activated) bgc = 1;
		}
		if(this.data.user.length >= this.data.config.userLimit){
			bgc = 2;
		}
		this.div.style.backgroundColor = bgc?"#DDDDDD":"";
		var R = "";
		R += "<div class='RoomColumn' style='width: 30px;'>"+this.data.id+"</div>";
		R += "<div class='RoomColumn' style='width: calc(100% - 310px);'>"+ROOM_STATUS[bgc]+this.data.title+"</div>";
		R += "<div class='RoomColumn' style='width: 100px; font-size: 13px; margin-top: 2px;'>"+RULE_NAME[this.data.config.rule]+"</div>";
		R += "<div class='RoomColumn' style='width: 120px;'>";
			R += this.data.config.item?"<font color='#0000FF'>아이템</font>":"<font color='#777777'>노템</font>";
			R += "&nbsp;"+this.data.config.round+"<label style='font-size: 12px;'>라운드</label>";
		R += "</div>";
		R += "<div class='RoomColumn' style='width: 60px;'>"+this.data.user.length+"<label style='font-size: 13px'>/"+this.data.config.userLimit+"</label></div>";
		this.div.innerHTML = R;
	}
});
var ChatDiv = classify({
	_ARG: function(data){
		this.data = data;
		this.div = document.createElement("div");
		this.div.className = "Chat";
		this.validateDiv();
	},

	data: null,
	div: null,

	validateDiv: function(){
		var R = "";
		switch(this.data.code){
			case USER_CONNECT:
			case ENTER:
				R += "<div class='ChatNoticeEnter'><label class='ChatUserName'>";
					R += this.data.data+"</label>님이 입장했습니다.";
				R += "</div>";
				break;
			case USER_DISCONNECT:
			case EXIT:
				R += "<div class='ChatNoticeExit'><label class='ChatUserName'>";
					R += this.data.data+"</label>님이 퇴장했습니다.";
				R += "</div>";
				break;
			case MASTER_CHANGED:
				R += "<div class='ChatNoticeExit'>방장이 <label class='ChatUserName'>";
					R += user[this.data.masterId].name+"</label>님으로 변경되었습니다.";
				R += "</div>";
				break;
			case BLOCKED:
				R += "<font color='#FF0000'>게임 이용이 "+(this.data.rest*0.001).toFixed(1)+"초간 제한됩니다.</font>";
				break;
			case EXIT:
				break;
			case CHAT:
				R += "<div class='ChatNormal'><div class='ChatWriter ChatUserName'>";
					//R += "<img class='ChatImage' src='https://graph.facebook.com/"+this.data.client.fb.id+"/picture' width='16px' height='16px' />";
					R += this.data.client.name+"</div><div class='ChatContent'>"+this.data.data+"</div>";
				R += "</div>";
				break;
			case SUDO_CHAT:
				R += "<div class='ChatSudo'><div class='ChatWriter ChatUserName' style='color: #AAAAFF;'>[운영자]&nbsp;";
					//R += "<img class='ChatImage' src='https://graph.facebook.com/"+this.data.client.fb.id+"/picture' width='16px' height='16px' />";
					R += this.data.client.name+"</div><div class='ChatContent'>"+this.data.data+"</div>";
				R += "</div>";
				break;
			default:
				console.warn("알 수 없는 유형:", this.data);
		}
		this.div.innerHTML = R;
	}
});
var MeaningDiv = classify({
	_ARG: function(data){
		this.data = data;
		this.div = document.createElement("div");
		this.div.className = "MeaningBlock";
		this.x = 0;
		this.div.style.left = this.x+"px";
		this.validateDiv();
	},

	data: null,
	x: null,
	div: null,

	moveX: function(val, animate){
		if(animate){
			this._aMoveX(val);
		}else{
			this.x = val;
			this.div.style.left = this.x+"px";
		}
	},
	_aMoveX: function(dest){
		var delta = dest-this.x;
		if(Math.abs(delta) < 1){
			this.x = dest;
		}else{
			this.x += delta*0.3;
			setTimeout(function(my){
				my._aMoveX(dest);
			}, 30, this);
		}
		this.div.style.left = Math.round(this.x)+"px";
	},
	validateDiv: function(){
		var R = "<label class='MeaningWord'>"+this.data.word+"</label><br />";
		if(this.data.mean.match(/<.+>/)){
			R = this.data.mean;
		}else{
			R += "<label class='MeaningText'>"+this.data.mean.replace(/,/g, ";&nbsp;")+"</label>";
		}
		this.div.innerHTML = R;
	}
});
var GameResultRankItem = classify({
	_ARG: function(data){
		this.data = data;
		this.div = document.createElement("div");
		this.div.className = "GameResultRankItem";
		this.validateDiv();
	},
	
	data: null,
	div: null,
	
	validateDiv: function(){
		this.div.className = "GameResultRankItem"+((this.data.id == my.id)?" MyGRItem":"");
		var R = "<div class='GRRank'>"+this.data.rank+"</div>";
		R += "<div class='GRName'>"+user[this.data.id].name+"</div>";
		R += "<div class='GRScore'>"+this.data.score+"</div>";
		this.div.innerHTML = R;
	}
});
var ItemEffectDiv = classify({
	_ARG: function(index){
		this.index = index;
		this.div = document.createElement("div");
		this.div.className = "ItemEffect";
		this.validateDiv();
	},
	
	index: null,
	div: null,
	
	validateDiv: function(){
		var R = ani.CON.ITEM_LIST[this.index]+"!";
		this.div.innerHTML = R;
	}
});