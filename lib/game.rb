class Game
	# 게임 정보 및 게임 진행 상황을 담당
	attr_accessor :client
	attr_accessor :ready
	attr_accessor :playing
	attr_accessor :score # 경험치가 아니다! 게임 한 판에서의 점수다!
	attr_accessor :toSent # 타임아웃 신호를 보냈는지 확인
	attr_accessor :item
	attr_accessor :bonusP # 보너스 (+)
	
	# item <Array> 내용
	#	:pass		0	자기 턴 넘기기
	#	:jump		1	다음 한 사람 건너뛰기
	#	:back		2	진행 방향 반대로 하기
	#	:double	3	자기가 한 번 더 입력하기
	
	def initialize(client)
		@client = client
		@playing = false
	end
	def start()
		case $rooms[@client.room].game[:type]
			when GameType::NORMAL
				@client.conquest.normalPlay[Con::RULE_INDEX[$rooms[@client.room].config[:rule]]] += 1
			when GameType::RANKED
				@client.conquest.rankedPlay[Con::RULE_INDEX[$rooms[@client.room].config[:rule]]] += 1
		end
		@score = 0
		@bonusP = 0.0
		@item = [0, 0, 0, 0]
		@ready = false
		@playing = true
	end
	def accept(data)
		@score += data[:score]
	end
	def boom()
		@score -= 100
		if @score < 0 then
			@score = 0
		end
	end
	def getItem(id)
		# 아이템은 { id = (턴을 끝내기 위해 소비한 시간)%6 } 인 것을 준다.
		# 순위 경기에서 이것이 정당할까?
		# -	순위 경기에서는 아이템을 못 쓰게 하거나
		# -	게임 시작 전 서로 공평하게 고르거나
		@item[id] += 1
	end
	def useItem(id)
		@item[id] -= 1
	end
	def flush(rank)
		# 클라이언트는 게임이 끝난 것으로 간주
		# ※방의 게임은 끝나지 않았다.
		# ※client.rb의 flush와 다르다.
		@playing = false
		res = {
			:id => @client.id,
			:score => @score,
			:rank => rank,
			:exp => getExp(@score),
			:ping => getPing(@score)
		}
		# 게임 결과 반영
		res[:exp] = res[:exp]*(1+($rooms[@client.room].users.length.to_f-rank)*0.5)+@bonusP
		res[:exp] *= 10
		case $rooms[@client.room].game[:type]
			when GameType::NORMAL
				@client.conquest.score += res[:exp]
				@client.conquest.ping += res[:ping]
			when GameType::RANKED
				# 랭겜은 항상 1:1이다.
				# 승점 변화량을 여기서 결정하고 있다.
				# ds: 점수 차이로 매기는 점수, dr: 승점 비율
				ri = Con::RULE_INDEX[$rooms[@client.room].config[:rule]]
				@client.conquest.score += res[:exp]*2
				@client.conquest.ping += res[:ping]*2
				ds = 0.3*Math.log(1+(@score - $clients[@client.rival].game.score).abs)
				dr = $clients[@client.rival].conquest.rating[ri] / @client.conquest.rating[ri]
				if rank == 1 then
					dr *= 10
				else
					dr *= -6
				end
				res[:rating] = ds*dr
				@client.conquest.rating[ri] += res[:rating]
		end
		publish("global", {
			:code => Con::USER_UPDATE,
			:data => @client.toObject
		})
		res
	end
	def toObject()
		{
			:score => @score,
			:ready => @ready,
			:item => @item
		}
	end
end