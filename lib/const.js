var WELCOME = "001";
var ERROR = "002";
var ALERT = "003";

var USER_CONNECT = "101";
var USER_DISCONNECT = "102";
var NEWBIE = "103";
var USER_UPDATE = "104";

var CHAT = "201";
var BLOCKED = "202";
var GET_STATUS = "203";

var ENTER = "301";
var EXIT = "302";
var ROOM_CREATE = "303";
var ROOM_DELETE = "304";
var ROOM_UPDATE = "305";
var MASTER_CHANGED = "306";

var READY = "401";
var TRY_START = "402";
var START = "403";
var WARN = "404";
var GAME_UPDATE = "405";
var FINISH = "406";
var TRY_END = "407";
var KICK = "408";
var START_PRACTICE = "409";
var ROUND_START = "411";
var ROUND_END = "412";
var TURN_START = "421";
var TURN_TIMEOUT = "422";
var TURN_PLAY = "423";
var ACCEPT = "424";
var WRONG_WORD = "425";
var NO_CHANCE = "426";
var AGAIN_WORD = "427";
var FIND_PLAYER = "431";
var FOUND_PLAYER = "432";
var CANCEL_MATCH = "433";
var HANDSHAKE = "434";
var RANK_START = "435";

var SUDO_CHAT = "501";

///////////////////////////////////////////////////////

var ITEM_NAME = ["패스", "끝말잇기", "찬스", "점프", "뒤로", "한번더"];
var ROOM_STATUS = [
	"",
	"<font color='#229922'>[진행 중]</font>&nbsp;",
	"<font color='#992222'>[꽉 참]</font>&nbsp;"
];
var WARN_MESSAGE = {
	newbieNameLength: "별명 길이가 4~16바이트 이내인지 확인해 보세요.",
	newbieNameChar: "별명에 특수 문자가 들어가지 않았는지 확인해 보세요.",
	newbieNameOccupied: "이미 사용 중인 별명입니다.",
	alreadyActivated: "이미 진행 중인 방입니다.",
	nullRoom: "없는 방입니다.",
	fullRoom: "방이 꽉 찼습니다.",
	haveBeenKicked: "강제 퇴장되어 다시 입장할 수 없습니다.",
	emptyTitle: "방 제목을 입력해 주세요.",
	wrongPassword: "암호가 일치하지 않습니다.",
	notYetReadied: "다른 참가자들이 아직 준비되지 않았습니다.",
	notEnoughPlayer: "2명 이상의 참가자가 모여야 합니다."
};
var BLOCKED_OPTION = {
	korean: "외래어 금지",
	substantive: "부사 금지"
};
var RULE_TYPE = {
	KT: "eng", ES: "eng",
	KS: "kor", KK: "kor",
	JS: "jp"
};
var RULE_INDEX = ["KT", "ES", "KS", "KK", "JS"];
var RULE_NAME = {
	KT: "영어 끄투",
	ES: "영어 끝말잇기",
	KS: "한국어 끝말잇기",
	KK: "한국어 쿵쿵따",
	JS: "시리토리"
};
var RULE_HELP = [
	"영어 단어의 <font color='#00AA00'>끝 두세 글자</font>를 잇는 방식입니다.<br />예) ap<u>ple</u> → <u>ple</u>a<u>se</u> → <u>se</u>ven",
	"영어 단어의 <font color='#00AA00'>끝 글자</font>를 잇는 방식입니다.<br />예) appl<u>e</u> → <u>e</u>rase<u>r</u> → <u>r</u>ed",
	"한국어 단어의 <font color='#00AA00'>끝 글자</font>를 잇는 방식입니다.<br />예) 창<u>고</u> → <u>고</u>등학<u>교</u> → <u>교</u>차로",
	"<font color='#00AA00'>세 글자인</font> 한국어 단어의 <font color='#00AA00'>끝 글자</font>를 잇는 방식입니다.<br />예) 강아<u>지</u> → <u>지</u>우<u>개</u> → <u>개</u>천절",
	"<font color='#00AA00'>ん으로 끝나지 않는</font> 일본어 단어를 잇는 방식입니다.<br />예) りん<u>ご</u> → <u>ご</u>うじょ<u>う</u> → <u>う</u>さぎ"
];
var RS_OPTIONAL = {
	KT: "-",
	ES: "-",
	KS: "<input name='RS_Foreign' type='checkbox' />외래어 금지<br /><input name='RS_Substantive' type='checkbox' />부사 금지",
	KK: "<input name='RS_Foreign' type='checkbox' />외래어 금지<br /><input name='RS_Substantive' type='checkbox' />부사 금지",
	JS: "-"
}
var USERSTAT_LOBBY = "lobby";
var USERSTAT_WAIT = "wait";
var USERSTAT_PLAYING = "playing";
var RIEUL_TO_NIEUN = [4449, 4450, 4452, 4453, 4454, 4457, 4458, 4459, 4460, 4462, 4463, 4464, 4465, 4467, 4468];
var NIEUN_TO_IEUNG = [4455, 4456, 4461, 4466, 4469];
function getGoalScore(level){
	var L = level?Math.round(5*level*level*level + 14*level*level + 69*level + 12):0;
	if(level == 20) L = 2147483647;
	return L;
}
function getLevel(score){
	var l = 0;
	var xp;
	do{
		l++;
		xp = getGoalScore(l);
	}while(score >= xp);
	return l;
}
function applyDueum(kor, rule){
	var r;
	var k;
	switch(rule){
		case "KS":
		case "KK":
			k = kor.charCodeAt() - 0xAC00;
			if(k < 0 || k > 11171){
				return null;
			}
			var code = [Math.floor((k/28)/21), Math.floor((k/28)%21), k%28];
			var chr = [code[0]+0x1100, code[1]+0x1161, code[2]+0x11A7];
			var changed = false;
			if(chr[0] == 4357){
				if(RIEUL_TO_NIEUN.indexOf(chr[1]) != -1){
					chr[0] = 4354;
				}else{
					chr[0] = 4363;
				}
				changed = true;
			}else if(chr[0] == 4354){
				if(NIEUN_TO_IEUNG.indexOf(chr[1]) != -1){
					chr[0] = 4363;
					changed = true;
				}
			}
			if(changed){
				chr[0] -= 0x1100;
				chr[1] -= 0x1161;
				chr[2] -= 0x11A7;
				r = String.fromCharCode(((chr[0]*21)+chr[1])*28+chr[2]+0xAC00);
			}
			break;
		case "JS":
			k = kor.charCodeAt();
			if(k < 12353 || k > 12542){
				return null;
			}
			if(k < 12449){
				r = String.fromCharCode(k+96);
			}else{
				r = String.fromCharCode(k-96);
			}
			break;
		default:
	}
	return r;
}