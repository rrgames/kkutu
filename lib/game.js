var isGaming = false;
var g;

function startGame(practice){
	RoomMenu.style.display = "none";
	GameMenu.style.display = "";
	RoomBox.style.display = "none";
	GameBox.style.display = "";
	MyTurnDiv.style.display = "none";
	GameResultBox.style.display = "none";
	GameItemBox.style.display = my.room.config.item?"":"none";
	isGaming = true;
	stopBGM(true);
	GameSubjectPanel.innerHTML = "잠시 후 시작합니다!";
	GameChainPanel.innerHTML = "";
	MeaningPanel.innerHTML = "";
	setTimeout(requestStartRound, 2000);
	g = {
		ef: setInterval(ef, 30),
		user: updateGameUser(),
		item: [0, 0, 0, 0],
		temScore: new Object()
	};
	room[my.room.id].user.forEach(function(item, index, my){
		g.temScore[item.id] = 0;
	});
	my.ready = true;
	aDark();
	requestToggleReady();
}
function closeGameBox(){
	RoomMenu.style.display = "";
	GameMenu.style.display = "none";
	RoomBox.style.display = "";
	GameBox.style.display = "none";
	GameResultBox.style.display = "none";
	isGaming = false;
	updateMe();
	playBGM("lobby");
	requestEndGame();
	drawRoomUserList();
	clearInterval(g.ef);
	clearTimeout(g.timer);
	if(my.room.config.rating){
		requestExitRoom();
	}
}
function finishGame(rank){
	g.timing = false;
	clearInterval(g.ef);
	GameSubjectPanel.innerHTML = "게임 종료!";
	setTimeout(showResult, 2000, rank);
}
function showResult(rank){
	GameResultBox.style.display = "";
	GameResultItemBox.innerHTML = "";
	var myRank;
	rank.forEach(function(item, index, _my){
		if(item.id == my.id){
			myRank = item.rank-1;
		}
		GameResultItemBox.appendChild(new GameResultRankItem(item).div);
	});
	switch(myRank){
		case 0:
			GameResultTitle.innerHTML = "<label style='color: #FFFF00; font-size: 24px;'>1</label>등";
			break;
		case 1:
			GameResultTitle.innerHTML = "<label style='color: #CCCCCC; font-size: 22px;'>2</label>등";
			break;
		case 2:
			GameResultTitle.innerHTML = "<label style='color: #B74444; font-size: 20px;'>3</label>등";
			break;
		default:
			GameResultTitle.innerHTML = "<label style='color: #666666; font-size: 18px;'>"+(myRank+1)+"</label>등";
	}
	var md = rank[myRank];
	g.result = md;
	g.result.rank = myRank;
	if(my.room.config.rating){
		var eq = (md.rating>=0)?("+"+md.rating.toFixed(1)):md.rating.toFixed(1);
		GameResultRatingGain.innerHTML = "<label style='font-size: 12px;'>ⓡ</label> "+eq;
	}else{
		GameResultRatingGain.innerHTML = "";
	}
	GameResultExpGain.innerHTML = "<label style='font-size: 12px;'>경험치</label> +"+Math.round(md.exp);
	GameResultPingGain.innerHTML = "<label style='font-size: 12px;'>ⓟ</label> +"+md.ping;
	// 애니메이팅을 할 때는 이미  my.data.conquest.score가 최신으로 반영되어 있으므로 빼야 한다.
	ani.geC = my.data.conquest.score-md.exp;
	ani.geMount = md.exp;
	aDrawBothExpBar(ani.geC);
	aResult();
}
function updateGameUser(){
	var R = new Array();
	GameUserBox.innerHTML = "";
	if(my.practice){
		my.pDiv = R[R.push(new GameUserDiv(my.room.getUserById(my.id), my.room))-1].div;
		GameUserBox.appendChild(my.pDiv);
		my.botDiv = R[R.push(new GameUserDiv({id: "bot"}, my.room))-1].div;
		GameUserBox.appendChild(my.botDiv);
	}else{
		var l = my.room.conseq.length;
		for(var i=0; i<l; i++){
			if(my.room.conseq[i]){
				if(!my.room.conseq[i]) continue;
				var o = new GameUserDiv(my.room.getUserById(my.room.conseq[i]), my.room);
				R.push(o);
				GameUserBox.appendChild(o.div);
			}else{
				my.room.conseq.splice(i, 1);
			}
		}
	}
	if(user[my.id].item){
		var il = document.getElementsByClassName("GameItemQuantity");
		for(i=0; i<4; i++){
			il[i].innerHTML = "x"+user[my.id].item[i];
		}
	}
	if(g){
		if(g.itemedUser){
			var puv = new ItemEffectDiv(g.itemedWhat);
			document.getElementById(g.itemedUser).appendChild(puv.div);
			aItemEffect(puv.div);
			delete g.itemedUser, g.itemedWhat;
		}
		if(g.recentPassed){
			var rpd = document.getElementById("GameUser_"+g.recentPassed);
			if(rpd) aDemagnify(rpd);
			delete g.recentPassed;
		}
	}
	return R;
}
function startRound(round){
	g.total = my.room.game.time;
	g.ctt = g.total;
	g.round = round;
	GameRoundPanel.innerHTML = "Round "+round+"<label style='font-size: 13px; color: #EEEEEE;'>&nbsp;/"+room[my.room.id].config.round+"</label>";
	MeaningPanel.innerHTML = "";
	GameChainPanel.innerHTML = "0";
	g.means = new Array();
	requestStartTurn();
}
function startTurn(data){
	my.room.game.subj = data.subj;
	var q = data.subj.replace(/[_]/gi, "");
	if(data.hasOwnProperty("subj2")){
		q += "("+data.subj2+")";
	}
	ani.wq = q;
	displaySubject(q);
	var cti = data.turn;
	g.myTurn = cti == my.id;
	if(g.myTurn){
		MyTurnDiv.style.display = "";
		ChatInput.focus();
	}
	g.mtt = data.time;
	g.tt = g.mtt;
	g.turn = cti;
	g.timing = true;
	playSound("T"+getTimeLevel(data.time), "Turn");
	if(g.timer != -1){
		clearTimeout(g.timer);
	}
	g.timer = setTimeout(requestTurnTimeout, data.time);
	var D = document.getElementById("GameUser_"+cti);
	aMagnify(D);
	D.className += " Pointed";
}
function getTimeLevel(time){
	// 0에서 10까지, 0이 느림
	return (15000-time)/1400
}
function displaySubject(arg, type){
	type = type || "start";
	GameSubjectPanel.innerHTML = arg;
	GameSubjectPanel.style.textDecoration = "inherit";
	switch(type){
		case "start":
			GameSubjectPanel.style.color = "#FFFF00";
			break;
		case "wrong":
			ani.wt = setTimeout(aWrong, 300, arg, 3);
		case "aWrong":
			GameSubjectPanel.style.textDecoration = "line-through";
			GameSubjectPanel.style.color = "#FF7777";
			break;
		case "end":
			aAccept(arg);
		case "normal":
			GameSubjectPanel.style.color = "#FFFFFF";
			break;
		default:
	}
}
function ef(){
	if(g.timing){
		g.ctt -= 30;
		g.tt -= 30;
		drawTimeBar();
	}
}
function accept(data){
	clearInterval(g.timer);
	my.pCount++;
	g.timer = -1;
	g.timing = false;
	stopSound("Turn");
	toggleItem(-1);
	if(my.practice){
		g.recentPassed = data.word;
		GameChainPanel.innerHTML = my.pCount;
	}else{
		g.recentPassed = g.turn;
		GameChainPanel.innerHTML = data.bonus.turn+1;
	}
	MyTurnText.value = "";
	MyTurnDiv.style.display = "none";
	if(data.word){
		displaySubject(data.word, "end");
		var al = g.means.length;
		var v = new MeaningDiv(data);
		MeaningPanel.appendChild(v.div);
		v.div.className += " LatestWord";
		aAppear(v.div);
		if(data.word.length >= 10){
			aEarthquake(data.word.length*0.5);
		}
		g.means.push(v);
		for(var i=0; i<al; i++){
			g.means[i].div.className = "MeaningBlock";
			g.means[i].moveX(g.means[i].x+v.div.offsetWidth+4, true);
		}
		var psv = document.getElementsByClassName("GameUserItem Pointed")[0];
		appearScore(data, psv);
		setTimeout(function(target, x, y, w, h){
			var vq = data.bonus.turn;
			while(vq > 0){
				GameStage.appendChild(new ScoreParticle("C", GameChainPanel.offsetLeft+GameChainPanel.offsetWidth+55, 123, x+w*0.5+Math.random()*10-20, y+h*0.5+Math.random()*10-20).div);
				vq -= 5;
			}
		}, 200, psv, psv.offsetLeft, psv.offsetTop, psv.offsetWidth, psv.offsetHeight);
		if(data.item != -1){
			g.itemedUser = psv.id;
			g.itemedWhat = data.item;
		}
	}else{
		aAccept(null);
		GameSubjectPanel.innerHTML = "패스!";
	}
	g.item.forEach(function(item, index, my){
		my[index] = 0;
	});
}
function appearScore(data, target){
	var o = {
		wc: window.innerWidth*0.5,
		tl: target.offsetLeft,
		tt: target.offsetTop,
		tw: target.offsetWidth,
		th: target.offsetHeight
	};
	var sv = new ScoreDiv(data);
	sv.left = o.tl-7;
	sv.top = o.tt+20;
	sv.validateDiv();
	setTimeout(function(){
		GameStage.appendChild(sv.div);
		aScore(sv);
	}, 400);
	if(!data.word) return;
	var sc = data.word.length*10;
	while(sc > 0){
		GameStage.appendChild(new ScoreParticle("N", o.wc, 100, o.tl+o.tw*0.5+Math.random()*10-20, o.tt+o.th*0.5+Math.random()*10-20).div);
		sc -= 10;
	}
	if(!data.bonus) return;
}
function wrong(data){
	displaySubject(data, "wrong");
	playSound("F", "F");
}
function drawTimeBar(){
	TotalTimeBar.style.width = "calc("+(g.ctt/g.total*100)+"% - 6px)";
	TotalTimeBar.innerHTML = Math.round(g.ctt*0.001)+"초";
	TurnTimeBar.style.width = "calc("+(g.tt/g.mtt*100)+"% - 6px)";
	TurnTimeBar.innerHTML = (g.tt*0.001).toFixed(1)+"초";
}
function drawTimeout(){
	g.timing = false;
	GameRoundPanel.innerHTML = "라운드 종료!";
	var D = document.getElementById("GameUser_"+g.turn);
	aDemagnify(D);
	MyTurnDiv.style.display = "none";
	D.className += " BoomPointed";
	appearScore({score: -100}, D);
	playSound("TO");
	TotalTimeBar.style.width = "0px";
	TurnTimeBar.style.width = "0px";
	setTimeout(requestStartRound, 2700);
}
function updateGame(data){
	var l = data.user.length;
	my.room.game.subj = data.game.subj;
	g.ctt = data.game.time;
	drawTimeBar();
	for(var i=0; i<l; i++){
		var o = my.room.user[i].game;
		var p = g.user[i].data.game;
		g.temScore[g.user[i].data.id] = p.score;
		for(var I in data.user[i].game){
			o[I] = data.user[i].game[I];
			p[I] = o[I];
		}
	}
	updateGameUser();
}
function toggleItem(index){
	if(!user[my.id].item) return;
	if(user[my.id].item[index] <= 0) return;
	var items = document.getElementsByClassName("GameItem");
	g.item.forEach(function(item, i, my){
		if(i == index){
			my[i] = my[i]?0:1;
			if(my[i]){
				if(index*(index-2) == 0){
					send();
				}
			}
		}else{
			my[i] = 0;
		}
		items[i].style.backgroundColor = my[i]?"rgba(150, 240, 180, 0.5)":"rgba(48, 150, 60, 0.5)";
	});
}
function foundTick(sec){
	gn("RG_Finding").innerHTML = "대기 시간 "+sec+"초";
	if(sec > 0){
		setTimeout(foundTick, 999, sec-1);
	}else{
		if(gn("RG_StartBtn").disabled){
			gn("RG_StartBtn").disabled = false;
			gn("RG_Found").style.display = "none";
			gn("RG_Finding").innerHTML = "상대를 찾고 있습니다...";
			requestFindPlayer(my.recentRule);
		}else{
			gn("RG_Rule").disabled = false;
			gn("RG_OKBtn").disabled = false;
			gn("RG_Found").style.display = "none";
			gn("RG_Finding").innerHTML = "취소되었습니다.";
		}
	}
}
function warnExitRoom(){
	if(my.practice){
		my.practice = false;
		clearTimeout(my.pTimer);
		closeGameBox();
		return;
	}
	if(confirm('게임 중 나갈 경우 불이익이 있습니다. 계속합니까?')){
		requestExitRoom();
	}
}