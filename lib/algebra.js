function prettyNumber(value) {
	var res = String(cut(value-Math.floor(value),2)).slice(1);
	var M = String(Math.floor(value));
	var Mt = 0;
	var ML = M.length;
	for (var I=0; I<ML-2; I++) {
		if ((ML-I-1)%3==0) {
			M = M.substr(0,I+1+Mt)+","+M.slice(I+1+Mt);
			Mt++;
		}
	}
	return M+res;
}
function cut(val, point){
	return Number(val.toFixed(point || 1));
}