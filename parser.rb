require 'open-uri'

def parseWord(word, rule, option)
	# 연습 모드를 넣으면서 인젝션 막을 필요가 생김
	r = nil
	cnt = 0
	isNew = false
	case Con::RULE_TABLE[rule]
		when "voca_KOR"
			res = $dbObj.query("SELECT `mean_KOR`, `count_#{rule}`, `option` FROM `#{Con::RULE_TABLE[rule]}` WHERE `word` = '#{word}';").fetch_row
			if res then
				r = res[0].force_encoding("UTF-8")
				if res[2] != nil then
					ops = res[2].split("")
					mObj = {
						:op => {
							:foreign => ops[0].to_i,
							:adverb => ops[1].to_i
						}
					}
				end
				cnt = res[1].to_f
			else
				mObj = getKoreanDef(word, "kor")
				if mObj then
					isNew = true
					r = mObj[:mean]
					puts "#{word} 학습: #{r}"
					$dbObj.query("INSERT INTO `#{Con::RULE_TABLE[rule]}` (`word`, `mean_KOR`, `option`) VALUES ('#{word}', '#{r.gsub(/'/, "＇")}', '#{mObj[:op][:foreign]}#{mObj[:op][:adverb]}00000000');")
				end
			end
			if mObj != nil then
				if option[:korean] && mObj[:op][:foreign] == 1 then
					return {
						:blocked => "korean"
					}
				end
				if option[:substantive] && mObj[:op][:adverb] == 1 then
					return {
						:blocked => "substantive"
					}
				end
			end
		when "voca_ENG", "voca_JAP"
			qs = "eng"
			if Con::RULE_TABLE[rule] == "voca_JAP" then
				qs = "jp"
			end
			res = $dbObj.query("SELECT `mean_KOR`, `count_#{rule}` FROM `#{Con::RULE_TABLE[rule]}` WHERE `word` = '#{word}';").fetch_row
			if res then
				r = res[0].force_encoding("UTF-8")
				cnt = res[1].to_f
			else
				mObj = getKoreanDef(word, qs)
				if mObj then
					isNew = true
					r = mObj[:mean]
					puts "#{word} 학습: #{r}"
					$dbObj.query("INSERT INTO `#{Con::RULE_TABLE[rule]}` (`word`, `mean_KOR`) VALUES ('#{word}', '#{r.gsub(/'/, "＇")}');")
				end
			end
	end
	# 전체 단어 사용 빈도:		서버를 켤 때 전역 변수($wordHit)로 저장해 뒀다가
	# 										매 시행마다 1만큼 더하면서 DB에 저장한다.
	# 해당 단어의 사용 빈도:	매 시행마다 1만큼 더하도록 질의를 한다.
	#										빈도는 뜻을 질의할 때 같이 구한다.

	# 반환값 r
	#	:word		단어
	#	:mean		뜻
	#	:usage		사용 비율 (<해당 단어의 사용 빈도>/$wordsHit)
	if r then
		# 아직 $wordsHit를 올리지 않았음
		usage = cnt/($wordsHit[rule]+1)
		puts "#{word}의 사용 비율: #{usage}"
		{
			:word => word,
			:mean => r,
			:usage => usage,
			:isNew => isNew
		}
	else
		nil
	end
end
def getChance(subj, rule, mean=false)
	if rule == Con::RULE_KT then
		if subj.length == 2 then
			subj = "_"+subj
		end
		cq = "(`word` LIKE '#{subj[-3..-1]}%' OR `word` LIKE '#{subj[-2..-1]}%')"
	else
		cq = "(`word` LIKE '#{subj[-1]}%'"
		dueum = applyDueum(subj[-1], rule)
		if dueum != nil then
			cq += " OR `word` LIKE '#{dueum}%'"
		end
		cq += ")"
		if rule == Con::RULE_KK then
			cq += " AND CHAR_LENGTH(`word`) = 3"
		end
	end
	if mean then
		res = $dbObj.query("SELECT `word`, `mean_KOR` FROM `#{Con::RULE_TABLE[rule]}` WHERE (#{cq}) AND `mean_KOR` IS NOT NULL LIMIT 100;")
		if res.num_rows < 1 then
			nil
		else
			word = nil
			cnt = 0
			pck = rand(res.num_rows)
			res.each_hash do |hash|
				if pck == cnt then
					word = hash
					word["word"] = word["word"].force_encoding("UTF-8");
					word["mean"] = word["mean_KOR"].force_encoding("UTF-8");
					break
				end
				cnt += 1
			end
			word
		end
	else
		res = $dbObj.query("SELECT `word` FROM `#{Con::RULE_TABLE[rule]}` WHERE (#{cq}) AND `mean_KOR` IS NOT NULL LIMIT 100;")
		if res.num_rows < 1 then
			nil
		else
			word = nil
			cnt = 0
			pck = rand(res.num_rows)
			res.each_hash do |hash|
				if pck == cnt then
					word = hash["word"]
					break
				end
				cnt += 1
			end
			word.force_encoding("UTF-8")
		end
	end
end
def getKoreanDef(word, lang)
	r = "_"
	pref = ""
	op = {
		:foreign => 0,
		:adverb => 0
	}
	open("http://m.dic.daum.net/search.do?q="+URI.escape(word)+"&dic="+lang) do |file|
		data = file.read
		
		sr = data.split("class=\"inner_tit\"")[1];
		if sr == nil then
			r = nil
		else
			if lang == "eng" then
				if sr[1..(sr.index("</span>"))].gsub(/\s/, "") != sr[1..(sr.index("</span>"))] then
					data = nil
					r = nil
				end
			end
			if lang == "kor" then
				fg = data.gsub(/<daum:word[^<]+<\/daum:word>/, "")
				fg = fg.match(/<span class="desc">[^<]+<\/span>/)
				if fg != nil then
					if fg[0].gsub(/<("[^"]*"|'[^']*'|[^'">])*>/, "").match(/[a-zA-Z]/) != nil then
						op[:foreign] = 1
					end
				end
				# 여기서 체언을 가려내지만, 부사도 통과할 수 있다.
				if data.match(/"tit_pos">부사<\//) != nil then
					op[:adverb] = 1
				elsif data.match(/"tit_pos">(의존 명사|명사|고유|고유 명사|인칭 대명사|지시 대명사|수사)<\//) == nil then
					r = nil
					data = nil
				end
			end
			if lang == "jp" then
				hanja = data.match(/<span class="phonetic font_dot">\s*[^\s]+\s*<\/span>/)
				if hanja != nil then
					pref = "[#{hanja[0].gsub(/<("[^"]*"|'[^']*'|[^'">])*>/, "").gsub(/\s/, "")}] "
				end
			end
			if data != nil then
				data = data.split("<div class=\"wrap_meaning\" >")[1]
				data = data.slice(1...(data.index("</div>"))).gsub(/&nbsp;/, " ")
				data = data.gsub(/<("[^"]*"|'[^']*'|[^'">])*>/, "")
				r = data
			end
		end
	end
	if r then
		{
			:mean => pref+r.split(/\s?[①-⑮]\s/)[1..-1].join(","),
			:op => op
		}
	else
		nil
	end
end
def applyDueum(kor, rule)
	r = nil
	case rule
		when Con::RULE_KS, Con::RULE_KK
			k = kor.ord - 0xAC00
			if k < 0 || k > 11171 then
				return nil
			end
			code = [(k/28)/21, (k/28)%21, k%28]
			char = [code[0]+0x1100, code[1]+0x1161, code[2]+0x11A7]
			changed = false
			if char[0] == 4357 then # ㄹ에서 ㄴ, ㅇ
				if Korean::RIEUL_TO_NIEUN.include?(char[1]) then
					char[0] = 4354
				else
					char[0] = 4363
				end
				changed = true
			elsif char[0] == 4354 then # ㄴ에서 ㅇ
				if Korean::NIEUN_TO_IEUNG.include?(char[1]) then
					char[0] = 4363
					changed = true
				end
			end
			if changed then
				char[0] -= 0x1100
				char[1] -= 0x1161
				char[2] -= 0x11A7
				r = "" << ((char[0]*21)+char[1])*28+char[2]+0xAC00
			end
		when Con::RULE_JS
			k = kor.ord
			if k < 12353 || k > 12542 then
				return nil
			end
			if k < 12449 then
				r = "" << k+96
			else
				r = "" << k-96
			end
	end
	r
end
def getRandomKoreanSubj(num)
	ra = Array.new
	res = $dbObj.query("SELECT SUBSTRING(`word`, 1, 1) FROM `voca_KOR`;")
	res.each do |val|
		ra.push(val[0].force_encoding("UTF-8"))
	end
	ra.sample(num)
end