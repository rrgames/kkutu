require 'em-websocket'
require 'mysql'
require 'json'
require 'uri'
require 'date'
require_relative 'lib/room'
require_relative 'lib/const'
require_relative 'lib/conquest'
require_relative 'lib/client'
require_relative 'lib/game'
require_relative 'lib/log'
require_relative 'lib/rank'

require_relative 'parser'

$rid = 100
$gen = Random.new
$rooms = {}
$clients = {} # key가 웹소켓 식별자
$users = {} # key가 페이스북 식별자
$dbObj = Mysql.new("localhost", "root", "dlalsgur", "kkutu")
$wordsHit = Hash.new
$dbObj.query("SELECT `name`, `value` FROM `global` WHERE `name` like 'wordsHit%';").each_hash do |item|
	$wordsHit[item["name"][9..-1]] = item["value"].to_i
end

def send(ws, data)
	ws.send_text(JSON.generate(data))
end
def publish(channel, data)
	json = JSON.generate(data)
	log "#{Time.now} PBL #{data[:code]} #{channel} #{json.bytesize}"
	global = channel == "global"
	$clients.each do |name, value|
		if value.socket.state.to_s == "closing" then
			next
		elsif global || (value.room == channel) then
			value.socket.send_text(json)
		end
	end
end
def getCMD(my, val)
	val = val << " "
	sv = val.index(" ")
	q = {
		:type => val[0...sv],
		:data => val[sv+1..-1]
	}
	res = nil
	case q[:type]
		when "전체"
			if my.fb[:sudo] then
				res = q
			end
	end
	res
end
def getRandomSubj(rule, num)
	res = Array.new
	while num > 0
		r = nil
		case rule
			when Con::RULE_KT
				a = "_"+Con::C_C[$gen.rand(0...Con::C_CL)]
				b = Con::C_V[$gen.rand(0...Con::C_VL)]
				r = a+b
			when Con::RULE_ES
				r = Con::C_A[$gen.rand(0...Con::C_AL)]
			when Con::RULE_KS, Con::RULE_KK
				res = getRandomKoreanSubj(num)
				break
			when Con::RULE_JS
				r = Con::C_J[$gen.rand(0...Con::C_JL)]
		end
		if r != nil then
			res.push(r)
		end
		num -= 1
	end
	res
end
def getTurnTime(val)
	# 음향 효과를 위해 턴 제한 시간은 11종류
	# T0			T1		T2			T10
	# 15000	13600	12200	...	1000
	1000+((val/TOTAL_TIME*10).to_i*1400)
end
def now()
	DateTime.now.strftime("%Q").to_f
end
def getPing(score)
	(score/4.16).to_i
end
def getExp(score)
	(score.to_f)/77.7
end
def getScore(data, time, bonusP=0)
	# time은 시간 계수 (쓴 시간)/(턴 제한 시간)
	data[:bonusP] = ((5-5*time).to_i)*10 + bonusP
	data[:bonusM] = 1
	# data
	#	:word		해당 단어
	#	:bonusP	+보너스 (기본 0)
	#	:bonusM	*보너스 (기본 1)
	(10 * data[:word].length.to_f * data[:bonusM] + data[:bonusP]).to_i
end
def isValidWord(word, subj, rule)
	# subj가 배열임에 유의
	r = true
	if word == nil then
		r = false
	else
		wdc = word.downcase()
		case rule
			when Con::RULE_KT
			# 영어 끄투
				r = r && (word.length >= 4)
				r = r && (((subj[0][1, 2] == word[0, 2]) || subj[0] == word[0, 3])) && (wdc.gsub(/[^a-z\s]/, "") == wdc)
			when Con::RULE_ES
			# 영어 끝말잇기
				r = r && (word.length >= 2)
				r = r && (subj[0] == word[0] && (wdc.gsub(/[^a-z\s]/, "") == wdc))
			when Con::RULE_KS
			# 한국어 끝말잇기
				r = r && (word.length >= 2)
				r = r && ((subj[0] == word[0]) || (subj[1] == word[0])) && (wdc.gsub(/[^가-힣]/, "") == wdc)
			when Con::RULE_KK
			# 한국어 쿵쿵따
				r = r && (word.length == 3)
				r = r && ((subj[0] == word[0]) || (subj[1] == word[0])) && (wdc.gsub(/[^가-힣]/, "") == wdc)
			when Con::RULE_JS
			# 시리토리
				r = r && (word.length >= 2)
				r = r && (word[-1] != "ん" && word[-1] != "ン")
				r = r && ((subj[0] == word[0]) || (subj[1] == word[0])) && (wdc.gsub(/[^ぁ-ヾ]/, "") == wdc)
		end
	end
	r
end
def welcome(client)
	ca = {}
	ra = {}
	$clients.each do |name, value|
		ca[name] = value.toObject(false)
	end
	$rooms.each do |name, value|
		ra[name] = value.toObject
	end
	send(client.socket, {
		:code => Con::WELCOME,
		:id => client.id,
		:me => client.toObject,
		:clients => ca,
		:rooms => ra
	})
	if client.fb[:sudo] then
		send(client.socket, {
			:code => Con::ALERT,
			:text => "당신은 관리자입니다!\n특별한 권리를 행사할 수 있습니다.\n채팅 창을 콘솔로 활용할 수 있습니다!"
		})
	end
	publish("global", {
		:code => Con::USER_CONNECT,
		:id => client.id,
		:name => client.name,
		:data => client.toObject(false)
	})
	puts "안내: 사용자 접속 성공 (#{client.name})"
end
def bad(o)
	# 나쁜 짓을 검사함
	res = false
	case o["code"]
		when Con::CHAT
			if o["data"] != nil then
				if o["data"].length > 200 then
					res = true
				end
			end
		when Con::ROOM_CREATE
			o["data"]["round"] = o["data"]["round"].to_i
			o["data"]["userLimit"] = o["data"]["userLimit"].to_i
			if (o["data"]["title"].length > 20) || (o["data"]["password"].length > 20) || (o["data"]["round"] < 1) || (o["data"]["round"] > 10) || (o["data"]["userLimit"] < 2) || (o["data"]["userLimit"] > 8) then
				res = true
			end
		when Con::ROOM_UPDATE
			o["data"]["round"] = o["data"]["round"].to_i
			o["data"]["userLimit"] = o["data"]["userLimit"].to_i
			if (o["data"]["title"].length > 20) || (o["data"]["password"].length > 20) || (o["data"]["round"] < 1) || (o["data"]["round"] > 10) || (o["data"]["userLimit"] < 2) || (o["data"]["userLimit"] > 8) then
				res = true
			end
		else
	end
	res
end
EM.run do
	puts "안내: 서버 작동 시작"
	puts "wordsHit = #{$wordsHit}"
	clientId = nil

	EM::WebSocket.start({
		:host => "0.0.0.0",
		:port => 12999,
		:secure => true,
		:tls_options => {
			:private_key_file => "/home/jjoriping/rrgames/ssl/key.pem",
			:cert_chain_file => "/home/jjoriping/rrgames/ssl/server.pem"
		}
	}) do |ws|
		ws.onopen do |handshake|
			clientId = handshake.headers["Sec-WebSocket-Key"]
			puts "안내: 사용자 접속 시도 (#{clientId})"
			fbURL = "https://graph.facebook.com/me?access_token="+handshake.query["token"]

			open(fbURL) do |file|
				# 페이스북 로그인 후 사용자 정보 등록
				data = JSON.parse(file.read)
				if data["error"] then
					puts "오류: oAuth 오류 #{data["error"].message}"
					next
				end
				if $users[data["id"]] != nil then
					# 동시 접속할 경우 기존에 접속한 세션은 종료시킴
					if $clients[$users[data["id"]]].fb[:sudo] != true then
						$clients[$users[data["id"]]].socket.close(3001)
					end
				end
				$clients[clientId] = Client.new(clientId)
				$clients[clientId].socket = ws
				$clients[clientId].fetch(data)
				$users[data["id"]] = clientId
				eval("ws.define_singleton_method(:client) { $clients['#{clientId}'] }")

				# 접속자 및 방 정보 반환 (신입 회원은 건너뜀 - client.rb 참고)
				if $clients[clientId].fb[:newbie] == false then
					welcome($clients[clientId])
				end
			end
		end

		ws.onerror do |e|
			puts "오류: #{e.message} #{e.backtrace}"
			log "#{Time.now} ERR #{e.message} #{e.backtrace}"
		end

		ws.onclose do |data|
			c = ws.client()
			puts "안내: 클라이언트 접속 종료 (#{c.id})"

			# 사용자 정보를 데이터베이스에 올리고 메모리에서 해제
			# ※ 서버가 중간에 오류가 나서 멈추면 백섭될 수 있다!
			c.flush()
			c.exit()
			# 순위 경쟁 큐에서 빼기
			if c.terminal != nil then
				$terminal["q#{c.terminal}"].delete_if do |item|
					item[:client].id == c.id
				end
			end
			$clients.delete(c.id)
			if data[:code] != 3001 then
				$users.delete(c.fb[:id])
			end
			publish("global", {
				:code => Con::USER_DISCONNECT,
				:id => c.id
			})
		end

		ws.onmessage do |msg|
			my = ws.client()
			begin
				o = JSON.parse(msg)
			rescue Exception => e
				log "#{Time.now} ERR Invalid msg '#{msg}'"
				next
			end
			log "#{Time.now} REQ #{o["code"]} #{msg}"
			myRoom = $rooms[my.room]
			
			curr = now
			# my.hack 검사
			deltaR = curr-my.hack[:recReq]
			if Con::BLOCK_THROUGH.include?(o["code"]) == false then
				if my.hack[:blocked] then
					# 10x(해킹 레벨) [초] 만큼 차단하고 해제
					if deltaR >= my.hack[:reqUnblockT] then
						my.hack[:blocked] = false
					else
						send(ws, {
							:code => Con::BLOCKED,
							:rest => my.hack[:reqUnblockT] - deltaR
						})
						next
					end
				end
				if deltaR <= 400 then
					# 0.4초 이내에 요청을 2번 이상 하면 점수 증가
					my.hack[:point] += 1
				elsif deltaR >= 800 then
					# 0.8초 이상 요청하지 않으면 초기화
					my.hack[:point] = 0
				end
				if my.hack[:point] >= 10 then
					#if my.hack[:blocked] then
						# 차단됐음에도 계속 요청했을 경우
						#ws.close()
					#else
						my.block()
						# 지속적으로 10회 이상 요청했을 경우
						if o["code"] != Con::CHAT then
							#ws.close()
						end
					#end
					next
				end
			end
			my.hack[:recReq] = curr
			if bad(o) then
				my.block()
				next
			end
			
			case o["code"]
				when Con::CHAT
				# 채팅 명령
					# 현재 턴인 사람이 요청을 보냈는지 확인
					# 0, 2번 아이템을 사용할 때 o["data"]값이 nil이다.
					# 토큰 유효성 검사
					if o["token"] != o["data"] then
						next
					end
					if o["data"] then
						o["data"] = o["data"].gsub(/&/, "&amp;").gsub(/</, "&lt;").gsub(/>/, "&gt;")
					end
					needV = my.game.playing && (myRoom.timer != nil)
					if needV then
						needV = needV && (myRoom.game[:conseq][myRoom.game[:turn]] == my.id)
					end
					if needV then
						if o["item"] == 0 then
							my.game.useItem(o["item"])
							myRoom.game[:ttt] = curr
							publish(my.room, {
								:code => Con::ACCEPT,
								:data => {
									:item => o["item"]
								}
							})
							myRoom.endTurn(nil, o["item"])
							needV = false
#						elsif o["item"] == 2 then
#							o["data"] = getChance(myRoom.game[:subj], myRoom.config[:rule])
#							if o["data"] == nil then
#								send(ws, {
#									:code => Con::NO_CHANCE
#								})
#								needV = false
#							else
#								my.game.useItem(o["item"])
#							end
						else
							if o["practice"] then
								qa = o["pSubj"]
							else
								qa = [myRoom.game[:subj], myRoom.game[:dueum]]
							end
							needV = needV && isValidWord(o["data"], qa, myRoom.config[:rule])
						end
					end
					needV = needV || o["practice"]
					if needV then
						if o["practice"] == false then
							if myRoom.game[:chain].include?(o["data"]) then
								publish(my.room, {
									:code => Con::AGAIN_WORD,
									:data => o["data"]
								})
								next
							end
							myRoom.game[:parsing] = true
							myRoom.game[:ttt] = curr
						end
						res = parseWord(o["data"].gsub(/(\\|&|'|")/, "뷁"), myRoom.config[:rule], myRoom.config[:option])
						if res == nil then
							publish(my.room, {
								:code => Con::WRONG_WORD,
								:data => o["data"]
							})
						elsif res[:blocked] != nil then
							# 추가 룰에 의해 막힘
							publish(my.room, {
								:code => Con::WRONG_WORD,
								:data => o["data"],
								:option => res[:blocked]
							})
						else
							bp = 0
							if o["practice"] == false then
								bt = myRoom.game[:chain].length
								bp = bt
								if myRoom.config[:rule] == "KT" then
									if o["data"][0...3] == myRoom.game[:subj] then
										bp += o["data"].length * 2
									end
								end
								tb = (myRoom.game[:ttt] - myRoom.timer)/myRoom.game[:tTime]
								ah = {
									:word => res[:word],
									:mean => res[:mean],
									:score => getScore(res, tb, bp),
									:bonus => {
										:time => tb,
										:turn => bt,
										:etcP => bp
									},
									:usage => res[:usage],
									:item => o["item"]
								}
								if ah[:item] != 2 then
									# 통계용 타자 측정
									my.conquest.typeTime[Con::RULE_INDEX[myRoom.config[:rule]]] += (myRoom.game[:ttt] - myRoom.timer)*0.001
									my.conquest.typeScore[Con::RULE_INDEX[myRoom.config[:rule]]] += ah[:word].length
								end
								if res[:isNew] then
									# 새로운 단어 발견에 대한 보너스 지급
									my.game.bonusP += 1.6
								end
								my.game.accept(ah)
								publish(my.room, {
									:code => Con::ACCEPT,
									:data => ah
								})
								myRoom.endTurn(o["data"], o["item"])
							else
								send(ws, {
									:code => Con::ACCEPT,
									:data => {
										:word => res[:word],
										:mean => res[:mean],
										:score => res[:word].length*10,
										:item => -1
									},
									:next => getChance(res[:word], myRoom.config[:rule], true)
								})
							end
							my.hack[:point] = [0, my.hack[:point]-1].max
						end
						myRoom.game[:parsing] = false
					else
						if o["data"] then
							cha = my.room
							dat = {
								:code => Con::CHAT,
								:data => o["data"],
								:client => {
									:id => my.id,
									:name => my.name
								}
							}
							if o["data"][0] == "/" then
								cmd = getCMD(my, o["data"][1..-1])
								if cmd != nil then
									case cmd[:type]
										when "전체"
											cha = "global"
											dat[:code] = Con::SUDO_CHAT
											dat[:data] = cmd[:data]
									end
								end
							end
							publish(cha, dat)
						end
					end
				when Con::GET_STATUS
					co = $clients[o["id"]]
					if co != nil then
						send(ws, {
							:code => Con::GET_STATUS,
							:data => co.toObject
						})
					end
				when Con::NEWBIE
				# 신입 회원의 초기 정보 설정
					if my.fb[:newbie] then
						bs = 0
						o["name"].each_byte do |c|
							if c > 255 then
								bs += 2
							else
								bs += 1
							end
						end
						if bs < 4 || bs > 16 then
							send(ws, {
								:code => Con::WARN,
								:text => Msg::NEWBIE_NAME_LENGTH
							})
						elsif o["name"].gsub(/[^a-zA-Z0-9가-힣 ]/, "") != o["name"] then
							send(ws, {
								:code => Con::WARN,
								:text => Msg::NEWBIE_NAME_CHAR
							})
						else
							my.setNewbie(o["name"])
						end
					end
				when Con::FIND_PLAYER
				# 순위 경쟁 큐에 등록
					if my.room == "lobby" then
						if my.terminal != nil then
							$terminal["q#{my.terminal}"].delete_if do |item|
								item[:client].id == my.id
							end
						end
						my.rival = nil
						addRankQueue(my, o["rule"])
					end
				when Con::HANDSHAKE
					if my.rival != nil then
						if my.rival[0] != "*" then
							if $clients[my.rival].rival[0] == "*" then
								# HANDSHAKE 성공, 방 파고 게임 시작
								$clients[my.rival].rival = my.id
								r = Room.new()
								$rooms[r.id] = r
								r.title = "#{my.name} vs #{$clients[my.rival].name}"
								r.config[:password] = "#{my.id}#{my.rival}"
								r.config[:item] = false
								r.config[:rule] = my.terminal
								r.config[:rating] = true
								r.config[:round] = 3
								r.config[:userLimit] = 2
								r.config[:conseq] = [my.id, my.rival].shuffle
								r.create(my.id)
								my.enter(r)
								$clients[my.rival].enter(r)
								send($clients[my.rival].socket, {
									:code => Con::ENTER,
									:id => my.rival,
									:roomId => r.id
								})
								send(ws, {
									:code => Con::ENTER,
									:id => my.id,
									:roomId => r.id
								})
								r.publishData()
								r.startGame(GameType::RANKED)
								publish(r.id, {
									:code => Con::START,
									:conseq => r.config[:conseq],
									:ranked => true
								})
							else
								my.rival = "*#{my.rival}"
							end
						end
					end
				when Con::CANCEL_MATCH
					if my.terminal != nil then
						if my.terminal.length < 10 then
							$terminal["q#{my.terminal}"].delete_if do |item|
								item[:client].id == my.id
							end
						end
						my.terminal = nil
					end
				when Con::ROOM_CREATE
				# 방 생성
					if o["data"]["title"].length < 1 then
						send(ws, {
							:code => Con::WARN,
							:text => Msg::EMPTY_TITLE
						})
					elsif my.room == "lobby" then
						r = Room.new()
						$rooms[r.id] = r
						r.title = o["data"]["title"]
						r.config[:password] = o["data"]["password"]
						r.config[:item] = o["data"]["item"]
						r.config[:rule] = o["data"]["rule"]
						r.config[:round] = o["data"]["round"].to_i
						r.config[:userLimit] = o["data"]["userLimit"].to_i
						if o["data"]["option"] != nil then
							r.config[:option][:korean] = o["data"]["option"]["korean"]
							r.config[:option][:substantive] = o["data"]["option"]["substantive"]
						end
						r.create(my.id)
						my.enter(r)
						send(ws, {
							:code => Con::ROOM_CREATE,
							:roomId => r.id,
							:title => r.title
						})
						r.publishData()
					end
				when Con::ROOM_UPDATE
				# 방 정보 변경 (방장 권한)
					if myRoom == nil then
						my.block()
					elsif my.id == myRoom.master then
						r = $rooms[my.room]
						r.title = o["data"]["title"]
						if o["data"]["password"] != "＊＊＊＊" then
							r.config[:password] = o["data"]["password"]
						end
						r.config[:item] = o["data"]["item"]
						r.config[:rule] = o["data"]["rule"]
						r.config[:round] = o["data"]["round"].to_i
						r.config[:userLimit] = o["data"]["userLimit"].to_i
						if o["data"]["option"] != nil then
							r.config[:option][:korean] = o["data"]["option"]["korean"]
							r.config[:option][:substantive] = o["data"]["option"]["substantive"]
						end
						r.publishData()
					end
				when Con::ENTER
				# 방에 입장
					if my.room == "lobby" then
						r = $rooms[o["data"]["id"]]
						if r == nil then
							send(ws, {
								:code => Con::WARN,
								:text => Msg::NULL_ROOM
							})
						elsif r.users.length >= r.config[:userLimit] then
							send(ws, {
								:code => Con::WARN,
								:text => Msg::FULL_ROOM
							})
						elsif r.game[:activated] then
							send(ws, {
								:code => Con::WARN,
								:text => Msg::ALREADY_ACTIVATED
							})
						elsif r.config[:password] != o["data"]["pw"] then
							send(ws, {
								:code => Con::WARN,
								:text => Msg::WRONG_PASSWORD
							})
						elsif r.blackList.index { |id| id == my.id } != nil then
							send(ws, {
								:code => Con::WARN,
								:text => Msg::HAVE_BEEN_KICKED
							})
						else
							my.enter(r)
							publish(r.id, {
								:code => Con::ENTER,
								:id => my.id,
								:roomId => r.id
							})
							r.publishData()
						end
					end
				when Con::READY
				# 준비
					my.game.ready = o["data"]
					$rooms[my.room].getUserById(my.id).game.ready = my.game.ready
					publish(my.room, {
						:code => Con::READY,
						:id => my.id,
						:data => my.game.ready
					})
				when Con::START_PRACTICE
				# 연습 요청
					send(ws, {
						:code => Con::START_PRACTICE,
						:subj => getRandomSubj(o["rule"], 5)
					})
				when Con::TRY_START
				# 시작 요청 (방장 권한)
					if my.id == myRoom.master then
						if myRoom.users.length < 2 then
							send(ws, {
								:code => Con::WARN,
								:text => Msg::NOT_ENOUGH_PLAYER
							})
						elsif myRoom.game[:activated] then
							# 게임이 이미 시작됨. 근데 요청을 보냈다?
						elsif myRoom.ready() then
							myRoom.startGame()
							publish(my.room, {
								:code => Con::START,
								:conseq => myRoom.game[:conseq]
							})
							myRoom.publishData()
						else
							send(ws, {
								:code => Con::WARN,
								:text => Msg::NOT_YET_READIED
							})
						end
					end
				when Con::TRY_END
				# 종료 공지 요청 (방장 권한)
					if my.id == myRoom.master then
						myRoom.game[:activated] = false
					end
				when Con::KICK
				# 강퇴 요청 (방장 권한)
					if my.id == myRoom.master then
						if myRoom.game[:activated] == false then
							num = myRoom.users.index do |item|
								item.id == o["id"]
							end
							if num != nil then
								myRoom.users[num].exit(true)
								myRoom.blackList.push(o["id"])
							end
						end
					end
				when Con::ROUND_START
				# 라운드 시작 요청 (방장 권한)
					if my.id == myRoom.master then
						myRoom.startRound()
					end
				when Con::TURN_START
				# 턴 시작 요청 - 제한 시간 시작 등 (방장 권한)
					if my.id == myRoom.master then
						myRoom.startTurn()
					end
				when Con::TURN_PLAY
					if my.id == myRoom.master then
						myRoom.playTurn()
					end
				when Con::TURN_TIMEOUT
					if myRoom.users[myRoom.game[:turn]].id != my.id then
						myRoom.sendTimeout(my)
					end
				when Con::EXIT
				# 방에서 퇴장
					my.exit()
				else
					puts "경고: 알 수 없는 유형 ("+o["code"]+")"
			end
		end
	end
end